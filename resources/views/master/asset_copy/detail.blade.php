@extends('base.main')
@section('title') Asset @endsection
@section('page_icon') <i class="fa fa-cube"></i> @endsection
@section('page_title') Detail Asset @endsection
@section('page_subtitle') detail @endsection
@section('menu')
    <div class="box box-solid" style="text-align:right;">
        <div class="box-body">
        	<a href="{{ route('asset.edit', $model->id) }}" class="btn btn-success" title="Edit Asset">
                <i class="fa fa-edit"></i> Update
            </a>
             {{-- onclick="return confirm('Anda yakin?')" --}}
            {{-- <a href="{{ route('asset.delete', $model->id) }}" class="btn btn-danger btn-delete2" title="Delete">
                <i class="fa fa-trash"></i> Delete
            </a> --}}
        	<a href="{{ route('asset.create') }}" class="btn btn-success" title="Create Asset">
                <i class="fa fa-plus"></i> Create
            </a>
            <a href="{{ route('asset.index') }}" class="btn btn-success" title="Manage Asset">
                <i class="fa fa-list"></i> Manage
            </a>
        </div>
    </div>
@endsection

@push('css')
<style>
.featured {
	background: #008d4c;
}	
</style>
@endpush

@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Asset</h3>
			</div>
			<div class="box-body">
				<table class="table table-striped">
					<tbody>
						<tr>
							<th>Name</th>
							<td>{{ $model->asset_name }}</td>
						</tr>
						<tr>
							<th>Category</th>
							<td>{{ $model->category }}</td>
						</tr>
						<tr>
							<th>Description</th>
							<td>{!! html_entity_decode($model->desc) !!}</td>
						</tr>
						<tr>
							<th>NJOP Price</th>
							<td>{{ "Rp " . number_format($model->price_njop,0,',','.') }}</td>
						</tr>
						<tr>
							<th>Market Price</th>
							<td>{{ "Rp " . number_format($model->price_market,0,',','.') }}</td>
						</tr>
						<tr>
							<th>Credit Tenor</th>
							<td>{{ $model->credit_tenor.' Month(s)' }}</td>
						</tr>
						<tr>
							<th>Investment Tenor</th>
							<td>{{ $model->invesment_tenor.' Month(s)' }}</td>
						</tr>
						<tr>
							<th>Term & Condition</th>
							<td>{{ $model->terms_conds }}</td>
						</tr>
					</tbody>
				</table>	
			</div>
		</div>

		<div class="box box-solid">
			<div class="box-body">
				<table class="table table-striped">
					<tbody>
						<tr>
							<th>Active</th>
							<td>{{ $model->active == 1 ? 'Ya' : 'Tidak' }}</td>
						</tr>
						<tr>
							<th>Created By</th>
							<td>{{ $uti->getUser($model->created_by) }}</td>
						</tr>
						<tr>
							<th>Created Date</th>
							<td>{{ date('d-m-Y H:i:s', strtotime($model->created_at)) }}</td>
						</tr>
						<tr>
							<th>Updated By</th>
							<td>{{ $uti->getUser($model->updated_by) }}</td>
						</tr>
						<tr>
							<th>Updated Date</th>
							<td>{{ date('d-m-Y H:i:s', strtotime($model->updated_at)) }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Owner</h3>
			</div>
			<div class="box-body">
				<table class="table table-striped">
					<tbody>
						<tr>
							<th>Name</th>
							<td>{{ $model->owner_name }}</td>
						</tr>
						<tr>
							<th>Identity Card NUmber</th>
							<td>{{ $model->owner_ktp_number }}</td>
						</tr>
						<tr>
							<th>Family Card Number</th>
							<td>{{ $model->owner_kk_number }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Detail</h3>
			</div>
			<div class="box-body">
				<table class="table table-striped">
					<tbody>
						@foreach($attributes as $attribute)
						<tr>
							<th>{{ $attribute->attr_name }}</th>
							<td>{{ $attribute->value }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>	
			</div>
		</div>
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Photo</h3>
			</div>
			<div class="box-body">
				<div class="row">
					@foreach($images as $image)
						<div class="col-md-4">
							<img src="/images/asset/{{ $image->photo_uri }}" class="img-thumbnail {!! $image->featured == 1 ? 'featured' : '' !!}">
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
@endsection