@extends('base.main')
@section('title') Asset @endsection
@section('page_icon') <i class="fa fa-cube"></i> @endsection
@section('page_title') Create Asset @endsection
@section('page_subtitle') list @endsection
@section('menu')
    <div class="box box-solid" style="text-align:right;">
        <div class="box-body">
            <a href="{{ route('asset.index') }}" class="btn btn-success" title="Manage Asset">
                <i class="fa fa-list"></i> Manage
            </a>
        </div>
    </div>
@endsection

@section('content')
<div class="box box-solid">
    {!! Form::model($model, [
        'route' => 'asset.store',
        'method'=> 'post',
        'enctype'   => 'multipart/form-data'
    ]) !!}
    <div class="box-body">
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{  $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <h4>Asset</h4>
        <div class="row">
        	<div class="col-md-8">
        		<div class="form-group">
                    <label for="asset_name" class="control-label">Name*</label>
                    {!! Form::text('asset_name', null, ['class'=>'form-control', 'id'=>'asset_name']) !!}

                    @if($errors->has('asset_name'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('asset_name') }}
                        </span>
                    @endif
                </div>
        	</div>
        	<div class="col-md-4">
                <div class="form-group">
                    <label for="category_asset_id" class="control-label">Category*</label>
                    {!! Form::select('category_asset_id', [''=>'- Select -'] + $category, null, ['class'=>'form-control dynamic', 'id'=>'category_asset_id']) !!}

                    @if($errors->has('category_asset_id'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('category_asset_id') }}
                        </span>
                    @endif
                </div>
        	</div>
        </div>
        <div class="row">
        	<div class="col-md-12">
        		<div class="form-group">
                    <label for="desc" class="control-label">Description*</label>
                    {!! Form::textarea('desc', null, ['class'=>'form-control textarea', 'id'=>'desc', 'rows'=>6]) !!}

                    @if($errors->has('desc'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('desc') }}
                        </span>
                    @endif
                </div>
        	</div>
        </div>
        <div class="row">
        	<div class="col-md-4">
        		<div class="form-group">
                    <label for="price_njop" class="control-label">NJOP Price*</label>
                    <div class="input-group">
                    	<span class="input-group-addon">
                    		Rp
                    	</span>
                    	{!! Form::text('price_njop', null, ['class'=>'form-control', 'id'=>'price_njop']) !!}
                    </div>

                    @if($errors->has('price_njop'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('price_njop') }}
                        </span>
                    @endif
                </div>
        	</div>
        	<div class="col-md-4">
        		<div class="form-group">
                    <label for="price_market" class="control-label">Market Price*</label>
                    <div class="input-group">
                    	<span class="input-group-addon">
                    		Rp
                    	</span>
                    	{!! Form::text('price_market', null, ['class'=>'form-control', 'id'=>'price_market']) !!}
                    </div>

                    @if($errors->has('price_market'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('price_market') }}
                        </span>
                    @endif
                </div>
        	</div>
        	<div class="col-md-4">
        		<div class="form-group">
                    <label for="credit_tenor" class="control-label">Credit Tenor*</label>
                    {!! Form::select('credit_tenor', [''=>'- Select -'] + $tenorCredit, null, ['class'=>'form-control', 'id'=>'credit_tenor']) !!}

                    @if($errors->has('credit_tenor'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('credit_tenor') }}
                        </span>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="invesment_tenor" class="control-label">Investment Tenor*</label>
                    {!! Form::select('invesment_tenor', [''=>'- Select -'] + $tenorInvestment, null, ['class'=>'form-control', 'id'=>'invesment_tenor']) !!}

                    @if($errors->has('invesment_tenor'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('invesment_tenor') }}
                        </span>
                    @endif
                </div>
        	</div>
        	<div class="col-md-4">
        		<div class="form-group">
                    <label for="terms_conds_id" class="control-label">Term & Conditions*</label>
                    {!! Form::select('terms_conds_id', [''=>'- Select -'] + $termsconds, null, ['class'=>'form-control', 'id'=>'terms_conds_id']) !!}

                    @if($errors->has('terms_conds_id'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('terms_conds_id') }}
                        </span>
                    @endif
                </div>
        	</div>
        	<div class="col-md-4">
        		<div class="form-group">
                    <label for="file_resume" class="control-label">File Resume*</label>
                    {!! Form::file('file_resume', ['accept'=>'.pdf']) !!}

                    @if($errors->has('file_resume'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('file_resume') }}
                        </span>
                    @endif
                </div>
        	</div>
        </div>
        <div class="row">
        	<div class="col-md-6" style="border-right: 1px solid #ccc;">
		        <label for="images" class="control-label">Photo*</label>

		        <div class="field_wrapper">
			        <div class="form-group img-form-group row">
                        <div class="col-md-7">
                            <input type="file" name="images[]" class="form-control" accept="image/x-png, image/jpeg">
                        </div>
                        <div class="col-md-3">
                            <label><input type="radio" name="featured[]" value="0" checked>&nbsp;Featured</label>
                        </div>
                        <div class="col-md-2">
                            <a href="javascript:void(0);" class="add_button btn btn-sm btn-success" title="Add Field"><i class="fa fa-plus"></i></a>
                        </div>
			        </div>
		        </div>
		        @if($errors->has('images'))
                    <span class="invalid-feedback" role="alert">
                        {{ $errors->first('images') }}
                    </span>
                @endif
        	</div>
            <div class="col-md-6">
                <label for="attr_name" class="control-label">Asset Details*</label>

                <div class="attributes_wrapper">
                    <div class="form-group attr-form-group row">
                        <div class="col-md-5">
                            <select name="attr_name[]" id="attr_name" class="form-control attr_name">
                                <option value="">- Select -</option>
                            </select>
                        </div>
                        <div class="col-md-5">
                            <input type="text" name="attr_value[]" class="form-control">
                        </div>
                        <div class="col-md-2">
                            <a href="javascript:void(0);" class="add_attr_button btn btn-sm btn-success" title="Add Field" style=""><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                </div>

                @if($errors->has('attr_name.*'))
                    <span class="invalid-feedback" role="alert">
                        {{ $errors->first('attr_name.*') }}
                    </span>
                @endif
                @if($errors->has('attr_value.*'))
                    <span class="invalid-feedback" role="alert">
                        {{ $errors->first('attr_value.*') }}
                    </span>
                @endif
            </div>
        </div>

        <hr>
        <h4>Owner</h4>
        <div class="row">
        	<div class="col-md-4">
                <div class="form-group">
                    <label for="owner_name" class="control-label">Full Name*</label>
                    {!! Form::text('owner_name', null, ['class'=>'form-control', 'id'=>'owner_name']) !!}

                    @if($errors->has('owner_name'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('owner_name') }}
                        </span>
                    @endif
                </div>
        	</div>
        	<div class="col-md-3">
                <div class="form-group">
                    <label for="owner_ktp_number" class="control-label">Identity Card Number*</label>
                    {!! Form::text('owner_ktp_number', null, ['class'=>'form-control', 'id'=>'owner_ktp_number']) !!}

                    @if($errors->has('owner_ktp_number'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('owner_ktp_number') }}
                        </span>
                    @endif
                </div>
        	</div>
        	<div class="col-md-3">
        		<div class="form-group">
                    <label for="owner_kk_number" class="control-label">Family Card Number*</label>
                    {!! Form::text('owner_kk_number', null, ['class'=>'form-control', 'id'=>'owner_kk_number']) !!}

                    @if($errors->has('owner_kk_number'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('owner_kk_number') }}
                        </span>
                    @endif
                </div>
        	</div>
        	<div class="col-md-2">
                <div class="form-group">
                    <label for="active" class="control-label">Active</label>
                    <div>
                        {!! Form::checkbox('active', null, null, ['id'=>'active']) !!} 
                    </div>

                    @if($errors->has('active'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('active') }}
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">
        {!! Form::submit('Create', ['class'=>'btn btn-primary pull-right']) !!}
    </div>
    {!! Form::close() !!}
</div>
@endsection

@push('scripts')
<script>
    $('.dynamic').change(function(){
        if($(this).val() != ''){
            var value = $(this).val(),
                token = $('input[name="_token"]').val();

            $.ajax({
                url : "{{ route('asset.fetch-attributes') }}",
                method : 'post',
                data : {
                    value : value,
                    _token : token
                },
                success : function(result){
                    $('.attr_name').html(result);
                }
            });
        }
    });

    // <div class="iradio_flat-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>

	var maxField = 10,
        addButton = $('.add_button'),
		addAttrButton = $('.add_attr_button'),
        wrapper = $('.field_wrapper'),
		attr_wrapper = $('.attributes_wrapper'),
        x = 0,
        y = 1;

	$(addButton).click(function(){
		if(x < maxField){
			x++;
			$(wrapper).append('<div class="form-group img-form-group row"><div class="col-md-7"><input type="file" name="images[]" class="form-control" accept="image/x-png, image/jpeg"></div><div class="col-md-3"><label><input type="radio" name="featured[]" value="'+x+'" >&nbsp;Featured</label></div><div class="col-md-2"><a href="javascript:void(0);" class="remove_button btn btn-sm btn-danger" title="Remove"><i class="fa fa-close"></i></a></div></div>');
		}
	});

	$(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).closest('.img-form-group').remove();
        // $(this).parent('div').remove(); //Remove field html
        // document.getElementsByClassName("form-group").remove();
        x--; //Decrement field counter
    });

    $(addAttrButton).click(function(){
        if(y < maxField){
            y++;
            $(attr_wrapper).append('<div class="form-group attr-form-group row"><div class="col-md-5"><select name="attr_name[]" class="form-control attr_name attr_'+y+'" id="attr_name"></select></div><div class="col-md-5"><input type="text" name="attr_value[]" class="form-control"></div><div class="col-md-2"><a href="javascript:void(0);" class="remove_attr_button btn btn-sm btn-danger" title="Remove"><i class="fa fa-close"></i></a></div></div>');

            var value = $('#category_asset_id').val();
            var token = $('input[name="_token"]').val();

            $.ajax({
                url : "{{ route('asset.fetch-attributes') }}",
                method : 'post',
                data : {
                    value : value,
                    _token : token
                },
                success : function(result){
                    $('.attr_'+y).html(result);
                }
            });
        }
    });

    $(attr_wrapper).on('click', '.remove_attr_button', function(e){
        e.preventDefault();
        $(this).closest('.attr-form-group').remove();
        // $(this).parent('div').remove(); //Remove field html
        // document.getElementsByClassName("form-group").remove();
        x--; //Decrement field counter
    });

</script>
@endpush