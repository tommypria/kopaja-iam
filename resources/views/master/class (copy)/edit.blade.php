@extends('base.main')
@section('title') Class @endsection
@section('page_icon') <i class="fa fa-user"></i> @endsection
@section('page_title') Edit Class @endsection

@section('menu')
    <div class="box box-solid" style="text-align:right;">
        <div class="box-body">
            <a href="{{ route('class.show', base64_encode($model->id)) }}" class="btn btn-success" title="Detail Class">
                <i class="fa fa-search"></i> Detail
            </a>
            <a href="{{ route('class.create') }}" class="btn btn-success" title="Create Class">
                <i class="fa fa-plus"></i> Create
            </a>
            <a href="{{ route('class.index') }}" class="btn btn-success" title="Manage Class">
                <i class="fa fa-list"></i> Manage
            </a>
        </div>
    </div>
@endsection

@section('content')
<div class="box box-solid">
    {!! Form::model($model, [
        'route' => ['class.edit', base64_encode($model->id)],
        'method'=> 'post',
        'enctype'   => 'multipart/form-data'
    ]) !!}

    <div class="box-body">
        <div class="box-body">
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{{  $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <p style="font-style: italic;">Click on plus( + ) button to add other language</p>
            <div class="field_wrapper">
                @php $count = 0; @endphp
                @foreach($data as $r)
                    <div class="form-group class-form-group row">
                        <div class="col-md-3">
                            @if($count <= 0) <label for="code" class="control-label">Language*</label> @endif
                            <select name="code[]" id="code" class="form-control attr_name">
                                <option value="">- Select -</option>
                                @foreach($language as $key => $value)
                                    @if($key == $r->code)
                                        <option value="{!! $key !!}" selected>{!! $value !!}</option>
                                    @else
                                        <option value="{!! $key !!}">{!! $value !!}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            @if($count <= 0)<label for="description" class="control-label">Name*</label> @endif
                            <input type="text" name="description[]" class="form-control" value="{!! $r->description !!}">
                        </div>
                        <div class="col-md-1" style="text-align: right;">
                            @if($count <= 0)
                                <img src="/images/class/{{ $r->image }}" width="40px" style="margin-top: 25px;" />
                            @else
                                <img src="/images/class/{{ $r->image }}" width="40px" />
                            @endif
                        </div>
                        <div class="col-md-3">
                            @if($count <= 0)<label for="icon" class="control-label">Icon*</label> @endif
                            <input type="file" name="image[]" accept="image/x-png, image/jpeg, image/jpg">
                        </div>
                        <div class="col-md-1">
                        @if($count <= 0)
                            <a href="javascript:void(0);" class="add_button btn btn-sm btn-success" title="Add Field" style="margin-top: 25px;"><i class="fa fa-plus"></i></a>
                        @else
                            <a href="javascript:void(0);" class="existed_attr_remove_button btn btn-sm btn-danger" title="Remove" data-id="{{ $r->id }}"><i class="fa fa-close"></i></a>
                        @endif
                        </div>
                    </div>
                    @php $count++; @endphp
                @endforeach
            </div>
        </div>
    <div class="box-footer">
        {!! Form::submit('Save', ['class'=>'btn btn-primary pull-right']) !!}
    </div>
    {!! Form::close() !!}
</div>
@endsection

@push('scripts')
<script>
    $('.existed_attr_remove_button').click(function(e){
        e.preventDefault();

        var id = $(this).data('id')
            token = $('input[name="_token"]').val();
        var ithis = $(this);

        swal({
            title : 'Are you sure ?',
            type : 'warning',
            showCancelButton : true,
            confirmButtonColor : '#3085d6',
            cancelButtonColor : '#d33',
            confirmButtonText : 'Yes, delete!'
        }).then((result)=>{
            if(result.value){

                $.ajax({
                    url : "{{ route('class.remove-lang') }}",
                    method : 'post',
                    data : {
                        id : id,
                        _token : token
                    },
                    success : function(r){
                        status = true;

                        ithis.closest('.class-form-group').remove();

                        swal({
                            type : 'success',
                            title : 'Success',
                            text : 'Deleted'
                        });
                    },
                    error : function(er){
                        swal({
                            type : 'error',
                            title : 'Failed',
                            text : 'Failed'
                        });
                    }
                });
            }
        });
    });

    var maxField = 10,
        addButton = $('.add_button'),
        wrapper = $('.field_wrapper'),
        x = 1;

    $(addButton).click(function(){
        if(x < maxField){
            x++;
            $(wrapper).append('<div class="form-group class-form-group row">'
                +'<div class="col-md-3">'
                    +'<select name="code[]" class="form-control attr_name attr_'+x+'" id="code"></select>'
                +'</div>'
                +'<div class="col-md-4">'
                    +'<input type="text" name="description[]" class="form-control">'
                +'</div>'
                +'<div class="col-md-4">'
                    +'<input type="file" name="image[]" accept="image/x-png, image/jpeg, image/jpg">'
                +'</div>'
                +'<div class="col-md-1">'
                    +'<a href="javascript:void(0);" class="remove_button btn btn-sm btn-danger" title="Remove"><i class="fa fa-close"></i></a>'
                +'</div>'
            +'</div>');

            var token = $('input[name="_token"]').val();

            $.ajax({
                url : "{{ route('class.fetch-language') }}",
                method : 'post',
                data : {
                    _token : token
                },
                success : function(result){
                    $('.attr_'+x).html(result);
                }
            });
        }
    });

    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).closest('.class-form-group').remove();
        // $(this).parent('div').remove(); //Remove field html
        // document.getElementsByClassName("form-group").remove();
        x--; //Decrement field counter
    });

</script>
@endpush