@extends('base.main')
@section('title') Class @endsection
@section('page_icon') <i class="fa fa-user"></i> @endsection
@section('page_title') Create Class @endsection

@section('menu')
    <div class="box box-solid" style="text-align:right;">
        <div class="box-body">
            <a href="{{ route('class.index') }}" class="btn btn-success" title="Manage Class">
                <i class="fa fa-list"></i> Manage
            </a>
        </div>
    </div>
@endsection

@section('content')
<div class="box box-solid">
    {!! Form::model($model, [
        'route' => 'class.store',
        'method'=> 'post',
        'enctype'   => 'multipart/form-data'
    ]) !!}

    <div class="box-body">
        <div class="box-body">
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{{  $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <p style="font-style: italic;">Click on plus( + ) button to add other language</p>
            <div class="field_wrapper">
            	<div class="form-group img-form-group row">
            		<div class="col-md-3">
            			<label for="code" class="control-label">Language*</label>
            			{!! Form::select('code[]', [''=>'- Select -'] + $language, 'IND', ['class'=>'form-control attr_name', 'id'=>'code']) !!}
            		</div>
            		<div class="col-md-4">
            			<label for="description" class="control-label">Name*</label>
            			<input type="text" name="description[]" class="form-control">
            		</div>
            		<div class="col-md-4">
            			<label for="image" class="control-label">Icon*</label>
            			<input type="file" name="image[]" accept="image/x-png, image/jpeg, image/jpg">
            		</div>
            		<div class="col-md-1">
            			<a href="javascript:void(0);" class="add_button btn btn-sm btn-success" title="Add Field" style="margin-top: 25px;"><i class="fa fa-plus"></i></a>
            		</div>
            	</div>
            </div>
        </div>
    </div>
    <div class="box-footer">
        {!! Form::submit('Create', ['class'=>'btn btn-primary pull-right']) !!}
    </div>
    {!! Form::close() !!}
</div>
@endsection

@push('scripts')
<script>

	var maxField = 10,
		addButton = $('.add_button'),
        wrapper = $('.field_wrapper'),
        x = 1;

    $(addButton).click(function(){
		if(x < maxField){
			x++;
			$(wrapper).append('<div class="form-group class-form-group row">'
				+'<div class="col-md-3">'
					+'<select name="code[]" class="form-control attr_name attr_'+x+'" id="code"></select>'
				+'</div>'
				+'<div class="col-md-4">'
					+'<input type="text" name="description[]" class="form-control">'
				+'</div>'
				+'<div class="col-md-4">'
					+'<input type="file" name="image[]" accept="image/x-png, image/jpeg, image/jpg">'
				+'</div>'
				+'<div class="col-md-1">'
					+'<a href="javascript:void(0);" class="remove_button btn btn-sm btn-danger" title="Remove"><i class="fa fa-close"></i></a>'
				+'</div>'
			+'</div>');

			var token = $('input[name="_token"]').val();

			$.ajax({
                url : "{{ route('class.fetch-language') }}",
                method : 'post',
                data : {
                    _token : token
                },
                success : function(result){
                    $('.attr_'+x).html(result);
                }
            });
		}
	});

    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).closest('.class-form-group').remove();
        // $(this).parent('div').remove(); //Remove field html
        // document.getElementsByClassName("form-group").remove();
        x--; //Decrement field counter
    });

</script>
@endpush