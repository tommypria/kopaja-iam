@extends('base.main')
@section('title') Class @endsection
@section('page_icon') <i class="fa fa-user"></i> @endsection
@section('page_title') Detail Class @endsection

@section('menu')
    <div class="box box-solid" style="text-align:right;">
        <div class="box-body">
        	<a href="{{ route('class.edit', base64_encode($model->id)) }}" class="btn btn-success" title="Edit Class">
                <i class="fa fa-edit"></i> Edit
            </a>
        	<a href="{{ route('class.create') }}" class="btn btn-success" title="Create Class">
                <i class="fa fa-plus"></i> Create
            </a>
            <a href="{{ route('class.index') }}" class="btn btn-success" title="Manage Class">
                <i class="fa fa-list"></i> Manage
            </a>
        </div>
    </div>
@endsection

@section('content')
<div class="box box-solid">
	<div class="box-body">
		<div class="box-body">
			<table class="table table-bordered table-striped">
				<tr>
					<th width="50px">No</th>
					<th>Language</th>
					<th>Name</th>
					<th width="200px">Icon</th>
				</tr>
				@php $no = 1; @endphp
				@foreach($data as $r)
				<tr>
					<td style="vertical-align: middle;">{{ $no++ }}</td>
					<td style="vertical-align: middle;">{{ $r->language }}</td>
					<td style="vertical-align: middle;">{{ $r->description }}</td>
					<td style="vertical-align: middle;"><img src="/images/class/{{ $r->image }}" width="45px" class="img-responsive"></td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>
@endsection