@extends('base.main')
@section('title') User Log Code @endsection
@section('page_icon') <i class="fa fa-cog"></i> @endsection
@section('page_title') User Log Code @endsection
@section('menu')
    <div class="box box-solid" style="text-align:right;">
        <div class="box-body">
            <a href="{{ route('log-code.create') }}" class="btn btn-success modal-show" title="Create">
                <i class="fa fa-plus"></i> Create
            </a>
        </div>
    </div>
@endsection

@section('content')
    <div class="box box-solid">
        <div class="box-body">
	        <div class="box-body">
	            <table id="datatable" class="table table-striped table-bordered">
	                <thead>
	                    <tr>
	                        <th>Code</th>
	                        <th>Module</th>
	                        <th>Message</th>
	                    </tr>
	                </thead>
	            </table>
	        </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('#datatable').DataTable({
            responsive : true,
            processing : true,
            serverSide : true,
            ajax: "{{ route('table.log-code') }}",
            columns: [
                {data : 'code', name : 'code'},
                {data : 'module', name : 'module'},
                {data : 'description', name : 'description'}
            ]
        });
    </script>
@endpush