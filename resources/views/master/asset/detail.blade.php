@extends('base.main')
@section('title') Asset @endsection
@section('page_icon') <i class="fa fa-cube"></i> @endsection
@section('page_title') Detail Asset @endsection
@section('page_subtitle') detail @endsection
@section('menu')
    <div class="box box-solid" style="text-align:right;">
        <div class="box-body">
        	<a href="{{ route('asset.edit', base64_encode($model->id)) }}" class="btn btn-success" title="Edit Asset">
                <i class="fa fa-edit"></i> Update
            </a>
            <a href="{{ route('asset.delete', base64_encode($model->id)) }}" class="btn btn-danger btn-delete2" title="Delete">
                <i class="fa fa-trash"></i> Delete
            </a>
        	<a href="{{ route('asset.create') }}" class="btn btn-success" title="Create Asset">
                <i class="fa fa-plus"></i> Create
            </a>
            <a href="{{ route('asset.index') }}" class="btn btn-success" title="Manage Asset">
                <i class="fa fa-list"></i> Manage
            </a>
        </div>
    </div>
@endsection

@section('content')
@if( !$model->isLanguageComplete() )
<div class="alert alert-warning alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4><i class="icon fa fa-warning"></i> Please add description for other language</h4>
	Asset won't be active if you not complete the description
</div>
@endif
<div class="nav-tabs-custom">
	<ul class="nav nav-tabs tabs-up" id="friends">
		<li>
			<a href="{{ route('asset.asset-pane', base64_encode($model->id) ) }}" data-target="#tab_asset" class="media_node active span" id="asset_tab" data-toggle="tabajax" rel="tooltip"><i class="fa fa-cubes"></i> Asset </a>
		</li>
		<li>
			<a href="{{ route('asset.rating-pane', base64_encode($model->id)) }}" data-target="#tab_rating" class="media_node span" id="rating_tab" data-toggle="tabajax" rel="tooltip"><i class="fa fa-star"></i> Rating</a>
		</li>
		<li>
			<a href="{{ route('asset.comment-pane', base64_encode($model->id)) }}" data-target="#tab_comment" class="media_node span" id="awaiting_request_tab" data-toggle="tabajax" rel="tooltip"><i class="fa fa-comment"></i> Comment</a>
		</li>
		<li>
			<a href="{{ route('asset.favorite-pane', base64_encode($model->id)) }}" data-target="#tab_favorit" class="media_node span" id="awaiting_request_tab" data-toggle="tabajax" rel="tooltip"><i class="fa fa-flag"></i> Favorite</a>
		</li>
		<li>
			<a href="{{ route('asset.investor-pane', base64_encode($model->id)) }}" data-target="#tab_investor" class="media_node span" id="awaiting_request_tab" data-toggle="tabajax_investor" rel="tooltip"><i class="fa fa-user"></i> Investor</a>
		</li>
	</ul>

	<div class="tab-content">
		<div class="tab-pane active" id="tab_asset"></div>
		<div class="tab-pane" id="tab_rating"></div>
		<div class="tab-pane" id="tab_comment"></div>
		<div class="tab-pane" id="tab_favorit"></div>
		<div class="tab-pane" id="tab_investor"></div>
	</div>
</div>
@endsection

@push('scripts')
<script>

	$('.active[data-toggle="tabajax"]').each(function(e) {
	    var $this = $(this),
	        loadurl = $this.attr('href'),
	        targ = $this.attr('data-target');

	    $.get(loadurl, function(data) {
	        $(targ).html(data);
	    });

	    $this.tab('show');
	    return false;
	});

	$('[data-toggle="tabajax"]').click(function(e) {
	    var $this = $(this),
	        loadurl = $this.attr('href'),
	        targ = $this.attr('data-target');

	    // if(!targ.hasChildNodes())
	    // {
		    $.get(loadurl, function(data) {
		        $(targ).html(data);
		    });
	    // }

	    $this.tab('show');
	    return false;
	});

	$('[data-toggle="tabajax_investor"]').click(function(e) {
	    var $this = $(this),
	        loadurl = $this.attr('href'),
	        targ = $this.attr('data-target');

	    $.get(loadurl, function(data) {
	        $(targ).html(data);

	        $('#datatable-investor').DataTable({
			    responsive : true,
			    processing : true,
			    serverSide : true,
			    pageLength : 20,
			    ajax: "{{ route('table.asset.investor', $model->id) }}",
			    columns: [
			        {data : 'investor', name : 'investor'},
			        {data : 'amount', name : 'amount'},
			        {data : 'tenor', name : 'tenor'},
			        {data : 'interest', name : 'interest'},
			        {data : 'daily_interest', name : 'daily_interest'},
			        {data : 'total_interest', name : 'total_interest'},
			        {data : 'status', name : 'status'}
			    ]
			});
	    });

	    $this.tab('show');
	    return false;
	});
</script>
@endpush

@push('css')
<style>
.featured {
	background: #008d4c;
}	
</style>
@endpush