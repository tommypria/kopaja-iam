<script>
	var areaChartData = {
	  	labels  : [],
	  	datasets: [
			{
				label               : 'Null',
				fillColor           : 'rgba(210, 214, 222, 1)',
				strokeColor         : 'rgba(210, 214, 222, 1)',
				pointColor          : 'rgba(210, 214, 222, 1)',
				pointStrokeColor    : '#c1c7d1',
				pointHighlightFill  : '#fff',
				pointHighlightStroke: 'rgba(220,220,220,1)',
				data                : []
			},
		]
	}

	var datanull = {
		labels : [],
		datasets : []
	}

	var months = new Array();
	months[0] = "";
	months[1] = "January";
	months[2] = "February";
	months[3] = "March";
	months[4] = "April";
	months[5] = "May";
	months[6] = "June";
	months[7] = "July";
	months[8] = "August";
	months[9] = "September";
	months[10] = "October";
	months[11] = "November";
	months[12] = "December";

	// var barChartCanvas 	= $('#barChart').get(0).getContext('2d')
	// var barChart 		= new Chart(barChartCanvas)
	var barChartOptions = {
	  //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
	  scaleBeginAtZero        : true,
	  //Boolean - Whether grid lines are shown across the chart
	  scaleShowGridLines      : true,
	  //String - Colour of the grid lines
	  scaleGridLineColor      : 'rgba(0,0,0,.05)',
	  //Number - Width of the grid lines
	  scaleGridLineWidth      : 1,
	  //Boolean - Whether to show horizontal lines (except X axis)
	  scaleShowHorizontalLines: true,
	  //Boolean - Whether to show vertical lines (except Y axis)
	  scaleShowVerticalLines  : true,
	  //Boolean - If there is a stroke on each bar
	  barShowStroke           : true,
	  //Number - Pixel width of the bar stroke
	  barStrokeWidth          : 2,
	  //Number - Spacing between each of the X value sets
	  barValueSpacing         : 5,
	  //Number - Spacing between data sets within X values
	  barDatasetSpacing       : 1,
	  //String - A legend template
	  legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
	  //Boolean - whether to make the chart responsive
	  responsive              : true,
	  maintainAspectRatio     : true
	}
	barChartOptions.datasetFill = false
	// barChart.Bar(datanull, barChartOptions);

	$('.btn-refresh').click(function(e){
		e.preventDefault();

		var form = $('#form-best-selling-asset'),
	        url = form.attr('action'),
	        method = form.attr('method');

		$.ajax({
			url : url,
			method : method,
			data : form.serialize(),
			success : function(response){
				
				var barChartCanvas 	= $('#barChart').get(0).getContext('2d')
				var barChart 		= new Chart(barChartCanvas)

				if(response.labels.length > 0)
				{
					var setdate = $('#txt-month').val() == '' ? 'Best selling asset on : '+$('#txt-year').val() : 'Best selling asset on : '+months[ $('#txt-month').val() ]+' '+$('#txt-year').val();

					document.getElementById('box-date').innerHTML = setdate;
					barChart.Bar(response, barChartOptions);
				}
				else
				{
					swal({
		                type : 'info',
		                title : 'Information',
		                text : 'Data not found'
		            });
				}
			},
			error : function(er){
				swal({
	                type : 'error',
	                title : 'Error',
	                text : 'Failed to retrieve data'
	            });
			}
		});
	});
</script>