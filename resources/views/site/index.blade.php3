@extends('base.main')
@section('title') Dashboard @endsection
@section('page_icon') <i class="fa fa-dashboard"></i> @endsection
@section('page_title') Dashboard @endsection
@section('page_subtitle') Monitor @endsection

@section('content')
<div class="row">
    <div class="col-md-3">
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>150</h3>
                <p>Total Investor</p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-md-3">
        <div class="small-box bg-blue">
            <div class="inner">
                <h3>150</h3>
                <p>Total Asset</p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-md-3">
        <div class="small-box bg-orange">
            <div class="inner">
                <h3>150</h3>
                <p>Total Investment</p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-md-3">
        <div class="small-box bg-maroon">
            <div class="inner">
                <h3>150</h3>
                <p>Total Balance</p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Area Chart</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="revenue-chart" style="height: 300px; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg height="300" version="1.1" width="510.5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.2.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><text x="51.515625" y="261" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">0</tspan></text><path fill="none" stroke="#aaaaaa" d="M64.015625,261H485.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="51.515625" y="202" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">7,500</tspan></text><path fill="none" stroke="#aaaaaa" d="M64.015625,202H485.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="51.515625" y="143" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">15,000</tspan></text><path fill="none" stroke="#aaaaaa" d="M64.015625,143H485.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="51.515625" y="84.00000000000003" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4.000000000000028" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">22,500</tspan></text><path fill="none" stroke="#aaaaaa" d="M64.015625,84.00000000000003H485.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="51.515625" y="25.00000000000003" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4.000000000000028" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">30,000</tspan></text><path fill="none" stroke="#aaaaaa" d="M64.015625,25.00000000000003H485.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="408.1681158869988" y="273.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2013</tspan></text><text x="220.72791995747266" y="273.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2012</tspan></text><path fill="#74a5c2" stroke="none" d="M64.015625,219.05493333333334C75.79465370595383,219.56626666666668,99.35271111786147,222.62345,111.13173982381531,221.10026666666667C122.91076852976914,219.57708333333335,146.46882594167678,209.1355825136612,158.24785464763062,206.86946666666668C169.89885043286756,204.6279825136612,193.20084200334142,204.88215,204.85183778857837,203.06986666666666C216.5028335738153,201.25758333333332,239.80482514428917,194.9129178506375,251.4558209295261,192.3712C263.2348496354799,189.80155118397084,286.79290704738764,182.51721666666668,298.57193575334145,182.6244C310.35096445929526,182.73158333333333,333.9090218712029,204.18057122040074,345.68805057715673,193.22866666666667C357.3390463623937,182.39580455373405,380.64103793286756,101.94395359116025,392.2920337181045,95.48533333333336C403.81499658262453,89.09768692449359,426.86092231166464,135.1380230769231,438.38388517618466,141.8436C450.16291388213847,148.69818974358975,473.7209712940462,147.7554,485.5,149.726L485.5,261L64.015625,261Z" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></path><path fill="none" stroke="#3c8dbc" d="M64.015625,219.05493333333334C75.79465370595383,219.56626666666668,99.35271111786147,222.62345,111.13173982381531,221.10026666666667C122.91076852976914,219.57708333333335,146.46882594167678,209.1355825136612,158.24785464763062,206.86946666666668C169.89885043286756,204.6279825136612,193.20084200334142,204.88215,204.85183778857837,203.06986666666666C216.5028335738153,201.25758333333332,239.80482514428917,194.9129178506375,251.4558209295261,192.3712C263.2348496354799,189.80155118397084,286.79290704738764,182.51721666666668,298.57193575334145,182.6244C310.35096445929526,182.73158333333333,333.9090218712029,204.18057122040074,345.68805057715673,193.22866666666667C357.3390463623937,182.39580455373405,380.64103793286756,101.94395359116025,392.2920337181045,95.48533333333336C403.81499658262453,89.09768692449359,426.86092231166464,135.1380230769231,438.38388517618466,141.8436C450.16291388213847,148.69818974358975,473.7209712940462,147.7554,485.5,149.726" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><circle cx="64.015625" cy="219.05493333333334" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="111.13173982381531" cy="221.10026666666667" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="158.24785464763062" cy="206.86946666666668" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="204.85183778857837" cy="203.06986666666666" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="251.4558209295261" cy="192.3712" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="298.57193575334145" cy="182.6244" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="345.68805057715673" cy="193.22866666666667" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="392.2920337181045" cy="95.48533333333336" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="438.38388517618466" cy="141.8436" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="485.5" cy="149.726" r="4" fill="#3c8dbc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><path fill="#eaf3f6" stroke="none" d="M64.015625,240.02746666666667C75.79465370595383,239.8072,99.35271111786147,241.35496666666666,111.13173982381531,239.1464C122.91076852976914,236.93783333333334,146.46882594167678,223.33676429872497,158.24785464763062,222.35893333333334C169.89885043286756,221.39173096539162,193.20084200334142,233.23263333333333,204.85183778857837,231.36626666666666C216.5028335738153,229.4999,239.80482514428917,209.2890577413479,251.4558209295261,207.428C263.2348496354799,205.54649107468123,286.79290704738764,214.43916666666667,298.57193575334145,216.39600000000002C310.35096445929526,218.35283333333334,333.9090218712029,232.37947613843352,345.68805057715673,223.08266666666668C357.3390463623937,213.88690947176687,380.64103793286756,148.22682412523022,392.2920337181045,142.42573333333334C403.81499658262453,136.6883907918969,426.86092231166464,170.47037838827842,438.38388517618466,176.92893333333336C450.16291388213847,183.53101172161175,473.7209712940462,190.23343333333335,485.5,194.66826666666668L485.5,261L64.015625,261Z" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></path><path fill="none" stroke="#a0d0e0" d="M64.015625,240.02746666666667C75.79465370595383,239.8072,99.35271111786147,241.35496666666666,111.13173982381531,239.1464C122.91076852976914,236.93783333333334,146.46882594167678,223.33676429872497,158.24785464763062,222.35893333333334C169.89885043286756,221.39173096539162,193.20084200334142,233.23263333333333,204.85183778857837,231.36626666666666C216.5028335738153,229.4999,239.80482514428917,209.2890577413479,251.4558209295261,207.428C263.2348496354799,205.54649107468123,286.79290704738764,214.43916666666667,298.57193575334145,216.39600000000002C310.35096445929526,218.35283333333334,333.9090218712029,232.37947613843352,345.68805057715673,223.08266666666668C357.3390463623937,213.88690947176687,380.64103793286756,148.22682412523022,392.2920337181045,142.42573333333334C403.81499658262453,136.6883907918969,426.86092231166464,170.47037838827842,438.38388517618466,176.92893333333336C450.16291388213847,183.53101172161175,473.7209712940462,190.23343333333335,485.5,194.66826666666668" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><circle cx="64.015625" cy="240.02746666666667" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="111.13173982381531" cy="239.1464" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="158.24785464763062" cy="222.35893333333334" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="204.85183778857837" cy="231.36626666666666" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="251.4558209295261" cy="207.428" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="298.57193575334145" cy="216.39600000000002" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="345.68805057715673" cy="223.08266666666668" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="392.2920337181045" cy="142.42573333333334" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="438.38388517618466" cy="176.92893333333336" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="485.5" cy="194.66826666666668" r="4" fill="#a0d0e0" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle></svg><div class="morris-hover morris-default-style" style="left: 345.933px; top: 57px; display: none;"><div class="morris-hover-row-label">2012 Q4</div><div class="morris-hover-point" style="color: #a0d0e0">
  Item 1:
  15,073
</div><div class="morris-hover-point" style="color: #3c8dbc">
  Item 2:
  5,967
</div></div></div>
            </div>
            <!-- /.box-body -->
          </div>
    </div>
    <div class="col-md-6">
        <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Donut Chart</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="sales-chart" style="height: 300px; position: relative;"><svg height="300" version="1.1" width="510.5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.2.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path fill="none" stroke="#3c8dbc" d="M255.25,243.33333333333331A93.33333333333333,93.33333333333333,0,0,0,343.4777551949771,180.44625304313007" stroke-width="2" opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 1;"></path><path fill="#3c8dbc" stroke="#ffffff" d="M255.25,246.33333333333331A96.33333333333333,96.33333333333333,0,0,0,346.31364732624417,181.4248826052307L387.5916327924656,195.6693795646951A140,140,0,0,1,255.25,290Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="none" stroke="#f56954" d="M343.4777551949771,180.44625304313007A93.33333333333333,93.33333333333333,0,0,0,171.53484627831412,108.73398312817662" stroke-width="2" opacity="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path><path fill="#f56954" stroke="#ffffff" d="M346.31364732624417,181.4248826052307A96.33333333333333,96.33333333333333,0,0,0,168.84400205154566,107.40757544301087L134.1620097954186,90.31165416754118A135,135,0,0,1,382.8651459070204,194.03833029452744Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="none" stroke="#00a65a" d="M171.53484627831412,108.73398312817662A93.33333333333333,93.33333333333333,0,0,0,255.22067846904883,243.333328727518" stroke-width="2" opacity="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path><path fill="#00a65a" stroke="#ffffff" d="M168.84400205154566,107.40757544301087A96.33333333333333,96.33333333333333,0,0,0,255.21973599126827,246.3333285794739L255.2075884998742,284.9999933380171A135,135,0,0,1,134.1620097954186,90.31165416754118Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="255.25" y="140" text-anchor="middle" font-family="&quot;Arial&quot;" font-size="15px" stroke="none" fill="#000000" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: Arial; font-size: 15px; font-weight: 800;" font-weight="800" transform="matrix(1.4524,0,0,1.4524,-115.616,-67.179)" stroke-width="0.6885230654761905"><tspan style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);" dy="5.5">Download Sales</tspan></text><text x="255.25" y="160" text-anchor="middle" font-family="&quot;Arial&quot;" font-size="14px" stroke="none" fill="#000000" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: Arial; font-size: 14px;" transform="matrix(1.9444,0,0,1.9444,-241.6302,-143.5556)" stroke-width="0.5142857142857143"><tspan dy="5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">12</tspan></text></svg></div>
            </div>
            <!-- /.box-body -->
          </div>
    </div>
</div>
    <div class="box box-solid">
        <div class="box-header">
            <h3 class="box-title">Monitor {{ Auth::user()->username }} {{ Auth::user()->sec_employee_id }}</h3>
        </div>
        <div class="box-body">
            test we
            @php
                $u = base_path().'/../image';
                echo $u;
            @endphp
            <img src="{{ base_path().'/../image/gedung-baja-tower-0.png' }}">
        </div>
    </div>
@endsection