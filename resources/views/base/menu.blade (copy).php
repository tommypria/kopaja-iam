@php
	use App\Utility;

	$uti = new Utility;
@endphp
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
<!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('images/employee/'.Auth::user()->photo) }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->full_name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            @php $uti->listMenu(); @endphp
{{-- Dashboard --}}
            <li class="treeview {{ Request::is('site') || Request::is('/') ? 'active' : null }}">
                <a href="{{ route('site.index') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Request::is('site.index') ? 'active' : null }}">
                    	<a href="{{ route('site.index') }}">
                            <i class="fa fa-circle-o"></i> Aset Terlaris
                        </a>
                    </li>
                    <li class="{{ Request::is('site.index') ? 'active' : null }}">
                    	<a href="{{ route('site.index') }}">
                            <i class="fa fa-circle-o"></i> Investor Tertinggi
                        </a>
                    </li>
                </ul>
            </li>
{{-- End Dashboard --}}
{{-- Master --}}
            <li class="treeview {{ Request::is('master/*') ? 'active' : null }}">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Master</span>
                </a>
                <ul class="treeview-menu">
                   
                    <li class="{{ Request::is('master/member') ? 'active' : null }}">
                        <a href="{{ route('member.index') }}">
                            <i class="fa fa-circle-o"></i> Member
                        </a>
                    </li>
					
                    <li class="{{ Request::is('master/bank') || Request::is('master/bank/*') ? 'active' : null }}">
                        <a href="{{ route('bank.index') }}">
                            <i class="fa fa-circle-o"></i> Bank
                        </a>
                    </li>
					
					<li class="treeview {{ Request::is('master/class') || Request::is('master/class/*') || Request::is('master/class_price') || Request::is('master/class_price/*') ? 'active' : null }}">
						<a href="#">
		                    <i class="fa fa-folder"></i> <span>Class</span>
		                </a>
						<ul class="treeview-menu">    
		                    <li class="{{ Request::is('master/class') ? 'active' : null }}">
		                        <a href="{{ route('class.index') }}">
		                            <i class="fa fa-circle-o"></i> Data Class
		                        </a>
		                    </li>
							
							 <li class="{{ Request::is('master/class_price') ? 'active' : null }}">
		                        <a href="{{ route('class-price.index') }}">
		                            <i class="fa fa-circle-o"></i> Price Class
		                        </a>
		                    </li>
						</ul>
					</li>
					
					<li class="treeview {{ 
						Request::is('master/countries') ||
						Request::is('master/countries/*') ||
						Request::is('master/province') ||
						Request::is('master/province/*') ||
						Request::is('master/regency') ||
						Request::is('master/regency/*') ||
						Request::is('master/district') ||
						Request::is('master/district/*') ||
						Request::is('master/village') ||
						Request::is('master/village/*') 
						? 'active' : null }}">
		                <a href="#">
		                    <i class="fa fa-folder"></i> <span>Address</span>
		                </a>
		                <ul class="treeview-menu">
		                    
							<!--li class="{{ Request::is('location/address') ? 'active' : null }}">
					                        <a href="/master/data_address">
					                            <i class="fa fa-circle-o"></i> Data Address
					                        </a>
					                    </li-->
										
							<li class="{{ Request::is('master/countries') || Request::is('master/countries/*') ? 'active' : null }}">
		                        <a href="{{ route('countries.index') }}">
		                            <i class="fa fa-circle-o"></i> Country
		                        </a>
		                    </li>
							
							<li class="{{ Request::is('master/province') || Request::is('master/province/*') ? 'active' : null }}">
		                        <a href="/master/province">
		                            <i class="fa fa-circle-o"></i> Province
		                        </a>
		                    </li>
							
							<li class="{{ Request::is('master/regency') || Request::is('master/regency/*') ? 'active' : null }}">
		                        <a href="/master/regency">
		                            <i class="fa fa-circle-o"></i> Regency
		                        </a>
		                    </li>
							
							<li class="{{ Request::is('master/district') || Request::is('master/district/*') ? 'active' : null }}">
		                        <a href="/master/district">
		                            <i class="fa fa-circle-o"></i> District
		                        </a>
		                    </li>
							
							<li class="{{ Request::is('master/village') || Request::is('master/village/*') ? 'active' : null }}">
		                        <a href="/master/village">
		                            <i class="fa fa-circle-o"></i> Village
		                        </a>
		                    </li>
		                </ul>
		            </li>

		            <li class="{{ Request::is('master/employee') || Request::is('master/employee/*') ? 'active' : null }}">
						<a href="{{ route('employee.index') }}">
							<i class="fa fa-circle-o"></i> Employee
						</a>
					</li>

					<li class="{{ Request::is('master/position') || Request::is('master/position/*') ? 'active' : null }}">
						<a href="{{ route('position.index') }}">
							<i class="fa fa-circle-o"></i> Position
						</a>
					</li>
					
					<li class="{{ Request::is('master/page') || Request::is('master/page/*') ? 'active' : null }}">
						<a href="{{ route('page.index') }}">
							<i class="fa fa-circle-o"></i> Page
						</a>
					</li>
					
					<li class="{{ Request::is('master/banner') || Request::is('master/banner/*') ? 'active' : null }}">
						<a href="{{ route('banner.index') }}">
							<i class="fa fa-circle-o"></i> Banner
						</a>
					</li>
					
					<li class="{{ Request::is('master/terms-conds') || Request::is('master/terms-conds/*') ? 'active' : null }}">
						<a href="{{ route('terms-conds.index') }}">
							<i class="fa fa-circle-o"></i> Term and Condition
						</a>
					</li>
					
					<li class="treeview {{ 
						Request::is('master/asset-category') ||
						Request::is('master/asset-category/*') ||
						Request::is('master/asset-attribute') ||
						Request::is('master/asset-attribute/*') ||
						Request::is('master/asset') ||
						Request::is('master/asset/*') 
						? 'active' : null }}">
						<a href="#">
		                    <i class="fa fa-folder"></i> <span>Asset</span>
		                </a>
						<ul class="treeview-menu">    
		                    <li class="{{ Request::is('master/asset-category') || Request::is('master/asset-category/*') ? 'active' : null }}">
		                        <a href="{{ route('asset-category.index') }}">
		                            <i class="fa fa-circle-o"></i> Category 
		                        </a>
		                    </li>
		                    <li class="{{ Request::is('master/asset-attribute') || Request::is('master/asset-attribute/*') ? 'active' : null }}">
		                        <a href="{{ route('asset-attribute.index') }}">
		                            <i class="fa fa-circle-o"></i> Attribute 
		                        </a>
		                    </li>
							 <li class="{{ Request::is('master/asset') || Request::is('master/asset/*') ? 'active' : null }}">
		                        <a href="{{ route('asset.index') }}">
		                            <i class="fa fa-circle-o"></i>  Asset
		                        </a>
		                    </li>
						</ul>
					</li>

					<li class="treeview {{ 
						Request::is('master/category-news') ||
						Request::is('master/category-news/*') ||
						Request::is('master/news') ||
						Request::is('master/news/*') ||
						Request::is('master/tag-news') ||
						Request::is('master/tag-news/*')
						? 'active' : null }}">
		                <a href="#">
		                    <i class="fa fa-folder"></i> <span>News</span>
		                </a>
		                <ul class="treeview-menu">
						
							<li class="{{ Request::is('master/category-news') || Request::is('master/category-news/*') ? 'active' : null }}">
		                        <a href="{{ route('category-news.index') }}">
		                            <i class="fa fa-circle-o"></i> Category News
		                        </a>
		                    </li>
							
							
		                    <li class="{{ Request::is('master/tag-news') || Request::is('master/tag-news/*') ? 'active' : null }}">
		                        <a href="{{ route('tag-news.index') }}">
		                            <i class="fa fa-circle-o"></i> Tag News
		                        </a>
		                    </li>
							
							<li class="{{ Request::is('master/news') || Request::is('master/news/*') ? 'active' : null }}">
		                        <a href="{{ route('news.index') }}">
		                            <i class="fa fa-circle-o"></i> Data News
		                        </a>
		                    </li>
							
		                </ul>
		            </li>
					
                    <li class="{{ Request::is('master/help') || Request::is('master/help/*') ? 'active' : null }}">
                        <a href="{{ route('help.index') }}">
                            <i class="fa fa-circle-o"></i> Help
                        </a>
                    </li>

                    <li class="{{ Request::is('master/privacy-policy') || Request::is('master/privacy-policy/*') ? 'active' : null }}">
						<a href="{{ route('privacy-policy.index') }}">
							<i class="fa fa-circle-o"></i> Privacy Policy
						</a>
					</li>
					
					<li class="{{ Request::is('master/contact-us') || Request::is('master/contact-us/*') ? 'active' : null }}">
						<a href="{{ route('contact-us.index') }}">
							<i class="fa fa-circle-o"></i> Contact Us
						</a>
					</li>

                </ul>
            </li>
{{-- End Master --}}	
{{-- Transaction --}}
			<li class="treeview {{ Request::is('transaction/*') ? 'active' : null }}">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Transaction</span>
                </a>
                <ul class="treeview-menu">
				
					<li class="{{ Request::is('transaction/investor') || Request::is('transaction/investor/*') ? 'active' : null }}">
                        <a href="{{ route('investor.index') }}">
                            <i class="fa fa-circle-o"></i> Investor Details
                        </a>
                    </li>					
				
                </ul>
            </li>
{{-- End Transaction --}}
{{-- Setting --}}
            <li class="treeview {{ Request::is('setting/*') ? 'active' : null }}">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Setting</span>
                </a>
                <ul class="treeview-menu">
				
					<li class="{{ Request::is('master/role') || Request::is('master/role/*') ? 'active' : null }}">
                        <a href="{{ route('role.index') }}">
                            <i class="fa fa-circle-o"></i> Role
                        </a>
                    </li>
					
                    <li class="{{ Request::is('setting/menu') || Request::is('setting/menu/*') ? 'active' : null }}">
                        <a href="{{ route('menu.index') }}">
                            <i class="fa fa-circle-o"></i> Menu
                        </a>
                    </li>
					
					<li class="{{ Request::is('setting/access-level') || Request::is('setting/access-level/*') ? 'active' : null }}">
                        <a href="{{ route('access-level.index') }}">
                            <i class="fa fa-circle-o"></i> Access Role
                        </a>
                    </li>
					
                </ul>
            </li>
{{-- End Setting --}}		
        </ul>
    </section>
<!-- /.sidebar -->
</aside>