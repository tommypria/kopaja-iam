@extends('base.main')
@section('title') Investor @endsection
@section('page_icon') <i class="fa fa-users"></i> @endsection
@section('page_title') Investor @endsection
@section('page_subtitle') list @endsection
@section('menu')
    <div class="box box-solid" style="text-align:right;">
        <div class="box-body">
            <a href="{{ route('investor.create') }}" class="btn btn-success" title="Create Investor">
                <i class="fa fa-plus"></i> Create
            </a>
        </div>
    </div>
@endsection

@section('content')
    <div class="box box-solid">
        {{-- <div class="box-header">
            <button class="btn btn-sm btn-danger btn-mass-delete"><i class="fa fa-trash"></i></button>
        </div> --}}
        <div class="box-body">
            <table id="datatable" class="table table-hover table-bordered">
                <thead>
                    <tr>
                        {{-- <th></th> --}}
                        {{-- <th>No</th> --}}
                        <th>Full Name</th>
                        <th width="100px">Active</th>
                        <th width="100px">Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('#datatable').DataTable({
            responsive : true,
            processing : true,
            serverSide : true,
            ajax: "{{ route('table.investor') }}",
            columns: [
                // {data : 'checkbox', name : 'checkbox'},
                // {data : 'DT_Row_Index', name : 'id'},
                {data : 'full_name', name : 'full_name'},
                // {data : 'description', name : 'description'},
                {data : 'active', name : 'active'},
                {data : 'action', name : 'action'}
            ]
        });
    </script>
@endpush