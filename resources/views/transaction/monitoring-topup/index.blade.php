@extends('base.main')
@section('title') Monitoring Balance In @endsection
@section('page_icon') <i class="fa fa-sign-in"></i> @endsection
@section('page_title') Monitoring Balance In @endsection
@section('page_subtitle') list @endsection
@push('css')
<style type="text/css">
.small-box { margin-bottom: 10px; }
</style>
@endpush
@section('content')
    @include('transaction.monitoring-topup.widget')

    <div class="box box-solid">
        <div class="box-header">
            <button class="btn bg-aqua btn-sm" id="btn-reload"><i class="fa fa-refresh"></i> Reload Data</button>
            <button class="btn bg-grey btn-sm" id="btn-clear-filter"><i class="fa fa-filter"></i> Clear Filter</button>
            <div class="box-tools pull-right">
                {{-- <button class="btn bg-grey btn-sm" id="btn-hint" data-toggle="modal" data-target="#modal-hint"><i class="fa  fa-info-circle"></i></button> --}}
                <div class="margin">
                    <span id="box-last-reload"></span>
                    <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        Menu
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="javascript:void(0)" class="btn-topup">Top Up</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="box-body">
          <div class="box-body">
              <table id="datatable" class="table table-hover table-bordered">
                  <thead>
                      <tr>
                          <th>Tran. Number</th>
                          <th>Date</th>
                          <th>Investor</th>
                          <th>Amount</th>
                          <th width="5%">Method</th>
                          <th width="5%">Status</th>
                          <th width="5%">Action</th>
                      </tr>
                  </thead>
              </table>
          </div>
        </div>
    </div>

    @include('transaction.monitoring-topup._form_recheck_reason')
    @include('transaction.monitoring-topup._form_reject_reason')
@endsection

@include('transaction.monitoring-topup.script')