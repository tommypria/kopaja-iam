$('body').on('click', '.modal-show', function(e){
    e.preventDefault();

    var me = $(this),
        url = me.attr('href'),
        title = me.attr('title'),
        id = me.data('id');
    
    $('#modal-title').text(title);
    $('#modal-btn-save').removeClass('hide').text(me.hasClass('edit') ? 'Update' : 'Create');
    
    $.ajax({
        url: url,
        dataType: 'html',
        success: function(response)
        {
            $('#modal').modal('show');
            $('#modal-body').html(response);
            $('input[name=investor_id]').val(id);
        },
        error : function(error)
        {
            $('#modal').modal('hide');
            swal({
                type : 'error',
                title : 'Error 401',
                text : 'Unauthorized action'
            });
        }
    });

});

$('#modal-btn-save').click(function(e){
    e.preventDefault();

    // console.log($('input[name=_method]').val());
    
    var form = $('#modal-body form'),
        url = form.attr('action'),
        method = form.attr('method');
        // method = $('input[name=_method]').val() == undefined ? 'post' : 'put';

    // console.log(method);

    form.find('.help-block').remove();
    form.find('.form-group').removeClass('has-error');
    
    $.ajax({
        url : url,
        method : method,
        data : form.serialize(),
        success : function(response){
            console.log('response : '+response);
            form.trigger('reset');
            $('#modal').modal('hide');
            $('#datatable').DataTable().ajax.reload();
            $('#datatable-internet_banking').DataTable().ajax.reload();
            $('#datatable-atm').DataTable().ajax.reload();

            swal({
                type : 'success',
                title : 'Success',
                text : 'Saved'
            });
        },
        error : function(e){
            console.log('error : '+e);
            var response = e.responseJSON;
            // console.log(response);
            if($.isEmptyObject(response) == false)
            {
                $.each(response.errors, function(key, value) {
                    $('#' + key)
                        .closest('.form-group')
                        .addClass('has-error')
                        .append('<span class="help-block">'+ value +'</span>')
                });
            }
        }
    });
});

$('body').on('click', '.btn-delete', function(e){
    e.preventDefault();

    var me = $(this),
        url = me.attr('href'),
        title = me.attr('title'),
        csrf_token = $('meta[name="csrf-token"]').attr('content');
    
    swal({
        title : 'Are you sure '+title+' ?',
        type : 'warning',
        showCancelButton : true,
        confirmButtonColor : '#3085d6',
        cancelButtonColor : '#d33',
        confirmButtonText : 'Ya, hapus!'
    }).then((result)=>{
        if(result.value){
            $.ajax({
                url : url,
                type : 'post',
                data : {
                    '_method': 'DELETE',
                    '_token' : csrf_token
                },
                success : function(r){
                    $('#datatable').DataTable().ajax.reload();
                    $('#datatable-internet_banking').DataTable().ajax.reload();
                    $('#datatable-atm').DataTable().ajax.reload();
                    swal({
                        type : 'success',
                        title : 'Success',
                        text : 'Data berhasil dihapus'
                    });
                },
                error : function(er){
                    if(er.status == 401)
                    {
                        swal({
                            type : 'error',
                            title : 'Error 401',
                            text : 'Unauthorized action'
                        });
                    }
                    else
                    {
                        swal({
                            type : 'error',
                            title : 'Failed',
                            text : 'Data gagal dihapus'
                        });
                    }
                }
            });
        }
    });

});

$('body').on('click', '.btn-delete2', function(e){
    e.preventDefault();

    var link = $(this).attr('href');

    swal({
        title : 'Are you sure ?',
        type : 'warning',
        showCancelButton : true,
        confirmButtonColor : '#3085d6',
        cancelButtonColor : '#d33',
        confirmButtonText : 'Ya, hapus!'
    }).then((result)=>{
        if(result.value){
            window.location = link;
        }
    });

});

$('body').on('click', '.btn-delete3', function(e){
    e.preventDefault();

    var link = $(this).attr('href');

    swal({
        title : 'Are you sure ?',
        type : 'warning',
        showCancelButton : true,
        confirmButtonColor : '#3085d6',
        cancelButtonColor : '#d33',
        confirmButtonText : 'Ya, hapus!'
    }).then((result)=>{
        if(result.value){
            $.ajax({
                url : link,
                type : 'get',
                success : function(r){
                    $('#datatable').DataTable().ajax.reload();
                    $('#datatable-internet_banking').DataTable().ajax.reload();
                    $('#datatable-atm').DataTable().ajax.reload();
                    
                    swal({
                        type : 'success',
                        title : 'Success',
                        text : 'Data berhasil dihapus'
                    });
                },
                error : function(er){
                    swal({
                        type : 'error',
                        title : 'Failed',
                        text : 'Data gagal dihapus'
                    });
                }
            });
        }
    });

});

$('body').on('click', '.btn-show', function(e){
    e.preventDefault();

    var me = $(this),
        url = me.attr('href'),
        title = me.attr('title'); 

    $('#modal-title').text(title);
    $('#modal-btn-save').addClass('hide');

    $.ajax({
        url :url,
        dataType : 'html',
        success : function(response){
            $('#modal').modal('show');
            $('#modal-body').html(response);
        },
        error : function(error)
        {
            $('#modal').modal('hide');
            swal({
                type : 'error',
                title : 'Error 401',
                text : 'Unauthorized action'
            });
        }
    });

});

// INVESTOR
$('body').on('click', '.inv-modal-show', function(e){
    e.preventDefault();

    var me = $(this),
        url = me.attr('href'),
        title = me.attr('title'),
        id = me.data('id');
    
    $('#inv-modal-title').text(title);
    $('#inv-modal-btn-save').removeClass('hide').text(me.hasClass('edit') ? 'Update' : 'Create');
    
    $.ajax({
        url: url,
        dataType: 'html',
        success: function(response)
        {
            $('#inv-modal-body').html(response);
            $('input[name=investor_id]').val(id);
        }
    });

    $('#inv-modal').modal('show');
});

$('#inv-modal-btn-save').click(function(e){
    e.preventDefault();
    
    var form = $('#modal-body form'),
        url = form.attr('action'),
        method = 'post';

    form.find('.help-block').remove();
    form.find('.form-group').removeClass('has-error');
    
    $.ajax({
        url : url,
        method : method,
        data : form.serialize(),
        success : function(response){
            form.trigger('reset');
            $('#inv-modal').modal('hide');
            $('#datatable-internet_banking').DataTable().ajax.reload();
            $('#datatable-atm').DataTable().ajax.reload();

            swal({
                type : 'success',
                title : 'Success',
                text : 'Data berhasil disimpan'
            });
        },
        error : function(e){
            var response = e.responseJSON;
            // console.log(response);
            if($.isEmptyObject(response) == false)
            {
                $.each(response.errors, function(key, value) {
                    $('#' + key)
                        .closest('.form-group')
                        .addClass('has-error')
                        .append('<span class="help-block">'+ value +'</span>')
                });
            }
        }
    });
});


/* *********************************
* Large Modal
* *********************************/
$('body').on('click', '.modal-show2', function(e){
    e.preventDefault();

    var me = $(this),
        url = me.attr('href'),
        title = me.attr('title'),
        id = me.data('id');
    console.log(url);
    console.log(title);
    console.log(id);
    
    $('#modal-title2').text(title);
    $('#modal-btn-save2').removeClass('hide').text(me.hasClass('edit') ? 'Update' : 'Create');
    
    $.ajax({
        url: url,
        dataType: 'html',
        success: function(response)
        {
            $('#modal2').modal('show');
            $('#modal-body2').html(response);
            $('input[name=investor_id]').val(id);
        },
        error : function(error)
        {
            $('#modal').modal('hide');
            swal({
                type : 'error',
                title : 'Error 401',
                text : 'Unauthorized action'
            });
        }
    });

});

$('#modal-btn-save2').click(function(e){
    e.preventDefault();

    // console.log($('input[name=_method]').val());
    
    var form = $('#modal-body2 form'),
        url = form.attr('action'),
        method = form.attr('method');
        // method = $('input[name=_method]').val() == undefined ? 'post' : 'put';

    // console.log(method);

    form.find('.help-block').remove();
    form.find('.form-group').removeClass('has-error');
    
    $.ajax({
        url : url,
        method : method,
        data : form.serialize(),
        success : function(response){
            console.log('response : '+response);
            form.trigger('reset');
            $('#modal2').modal('hide');
            $('#datatable').DataTable().ajax.reload();
            $('#datatable-internet_banking').DataTable().ajax.reload();
            $('#datatable-atm').DataTable().ajax.reload();

            swal({
                type : 'success',
                title : 'Success',
                text : 'Saved'
            });
        },
        error : function(e){
            console.log('error : '+e);
            var response = e.responseJSON;
            // console.log(response);
            if($.isEmptyObject(response) == false)
            {
                $.each(response.errors, function(key, value) {
                    $('#' + key)
                        .closest('.form-group')
                        .addClass('has-error')
                        .append('<span class="help-block">'+ value +'</span>')
                });
            }
        }
    });
});

$('body').on('click', '.btn-show2', function(e){
    e.preventDefault();

    var me = $(this),
        url = me.attr('href'),
        title = me.attr('title'); 

    $('#modal-title2').text(title);
    $('#modal-btn-save2').addClass('hide');

    $.ajax({
        url :url,
        dataType : 'html',
        success : function(response){
            $('#modal2').modal('show');
            $('#modal-body2').html(response);
        },
        error : function(error)
        {
            $('#modal2').modal('hide');
            swal({
                type : 'error',
                title : 'Error 401',
                text : 'Unauthorized action'
            });
        }
    });

});