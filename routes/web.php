<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Auth::routes();
Route::group(['middleware'=>['auth']], function(){

    // Route::get('signout', 'WebController@destroy');
    // Root
    Route::get('/', 'SiteController@index');
    // Site
    Route::resource('site', 'SiteController');
    // Dashboard
    Route::group(['prefix'=>'dashboard'], function(){
    	// Asset Terlaris
    	Route::get('asset-best-selling', 'SiteController@assetReport')->name('site.asset-best-selling');
    	Route::post('asset-best-selling/data', 'SiteController@bestSellingAsset')->name('site.data-asset-best-selling');
    	Route::get('asset-best-selling/investor/{id}', 'SiteController@bestSellingAssetInvestor')->name('site.investor-asset-best-selling');
        Route::get('table/asset-best-selling/{id}', 'SiteController@dataTableInvestor')->name('table.investor-asset-best-selling');
    	
    	// Investor Tertinggi
    	Route::get('highest-investment', 'DashboardController@dashboardHighestInvestment')->name('dashboard.highest-investment');
    	Route::post('highest-investment/data', 'DashboardController@dataHighestInvestment')->name('dashboard.data-highest-investment');
    	Route::get('highest-investment/asset', 'DashboardController@assetHighestInvestment');
    	Route::get('table/highest-investment/{id}', 'DashboardController@tableHighestInvestmentAsset');

        /* --------------- MAIN DASHBOARD -------------------- */
        // Route::get()->name();
    });

    // Master
    Route::group(['prefix'=>'master'], function(){
        // Role
        Route::resource('role', 'SecRoleController');
        Route::get('table/role', 'SecRoleController@dataTable')->name('table.role');
        
        /* --------------- MEMBER -------------------- */
        Route::resource('member', 'MstMemberController');
        Route::get('table/member', 'MstMemberController@dataTable')->name('table.member');
        Route::get('table/memberlang/{id}', 'MstMemberController@memberLangTable')->name('table.memberlang');
        Route::get('member/lang/{id}', 'MstMemberController@newLanguage')->name('member.lang');
        Route::get('member/lang/edit/{id}', 'MstMemberController@editLanguage')->name('member.editlang');
        Route::get('member/lang/destroy/{id}', 'MstMemberController@destroyLanguage')->name('member.destroylang');
        Route::get('member/detail/{id}', 'MstMemberController@detail')->name('member.detail');
        Route::get('table/member-lang/{id}', 'MstMemberController@dataTableLang')->name('table.member-lang');
        Route::get('member/delete/{id}', 'MstMemberController@delete')->name('member.delete');
        Route::get('member/edit-language/{id}', 'MstMemberController@editLanguage')->name('member.edit-lang');
        
        /* --------------- CLASS -------------------- */
        Route::resource('class', 'MstClassController');
        Route::get('table/class', 'MstClassController@dataTable')->name('table.class');
        Route::get('table/class-detail/{id}', 'MstClassController@dataTableDetail')->name('table.class-detail');
        Route::post('class/fetch-language', 'MstClassController@fetchLanguage')->name('class.fetch-language');
        Route::get('class/remove-language/{id}', 'MstClassController@removeLanguage')->name('class.remove-lang');
        Route::get('class/edit-language/{id}', 'MstClassController@editLanguage')->name('class.edit-lang');
        Route::post('class/update-language/{id}', 'MstClassController@updateLanguage')->name('class.update-lang');
        Route::get('class/delete/{id}', 'MstClassController@delete')->name('class.delete');
        // Class Price
        Route::resource('class-price', 'MstClassPriceController');
        Route::get('table/class-price', 'MstClassPriceController@dataTable')->name('table.MstClassPrice');
        
        /* --------------- BANK -------------------- */
        Route::resource('bank', 'MstBankController');
        Route::get('table/bank', 'MstBankController@dataTable')->name('table.bank');

        // Asset Category
        Route::resource('asset-category', 'MstAssetCategoryController');
        Route::get('table/asset-category', 'MstAssetCategoryController@dataTable')->name('table.asset-category');
        Route::get('table/asset-category-detail/{id}', 'MstAssetCategoryController@dataTableDetail')->name('table.asset-category-detail');
        Route::get('asset-category/delete/{id}', 'MstAssetCategoryController@delete')->name('asset-category.delete');
        // Help / FAQ
        Route::resource('help', 'MstHelpController');
        Route::get('table/help', 'MstHelpController@dataTable')->name('table.help');
        Route::get('help/delete/{id}', 'MstHelpController@delete')->name('help.delete');
        Route::post('help/remove-image', 'MstHelpController@removeImage')->name('help.remove-image');
        Route::get('help/create-new/id/{id}/lg/{lg}','MstHelpController@createNew')->name('help.create-new');
        Route::post('help/store-new', 'MstHelpController@storeNew')->name('help.store-new');
        Route::get('help/edit-new/id/{id}','MstHelpController@editNew')->name('help.edit-new');
        Route::put('help/update-new/{id}', 'MstHelpController@updateNew')->name('help.update-new');
        
        /* --------------- EMPLOYEE -------------------- */
        Route::resource('employee', 'MstEmployeeController');
        Route::get('table/employee', 'MstEmployeeController@dataTable')->name('table.employee');
        Route::post('employee/fetch', 'MstEmployeeController@fetch')->name('employee.fetch');
        Route::get('employee/delete/{id}', 'MstEmployeeController@delete')->name('employee.delete');
        // Page
        Route::resource('page', 'MstPageController');
        Route::get('table/page', 'MstPageController@dataTable')->name('table.page');
        // Terms & Conditions
        Route::resource('terms-conds', 'MstTermsCondsController');
        Route::get('table/terms-conds', 'MstTermsCondsController@dataTable')->name('table.terms-conds');
        Route::get('table/terms-conds-lang/{id}', 'MstTermsCondsController@dataTableLang')->name('table.terms-conds-lang');
        Route::get('terms-conds/delete/{id}', 'MstTermsCondsController@delete')->name('terms-conds.delete');
        Route::get('terms-conds/detail/{id}', 'MstTermsCondsController@detail')->name('terms-conds.detail');
        Route::get('terms-conds/edit_detail/{id}', 'MstTermsCondsController@edit_detail')->name('terms-conds.edit_detail');
        
        /* --------------- BANNER -------------------- */
        Route::resource('banner', 'MstBannerController');
        Route::get('table/banner', 'MstBannerController@dataTable')->name('table.banner');
        Route::get('banner/setposition/{id}','MstBannerController@setposition')->name('banner.setposition');
        Route::get('banner/delete/{id}', 'MstBannerController@delete')->name('banner.delete');
        Route::post('banner/set','MstBannerController@set')->name('banner.set');
        Route::post('banner/fetch', 'MstBannerController@fetch')->name('banner.fetch');
        Route::get('banner/deletepos/b/{b}/p/{p}', 'MstBannerController@deleteposition')->name('banner.deletepos');
        Route::get('table/banner/position/{id}', 'MstBannerController@positionDataTable')->name('table.banner.position');
        Route::post('banner/get-position', 'MstBannerController@getPosition')->name('banner.get-position');
        Route::get('banner/create-new/id/{id}/lg/{lg}','MstBannerController@createNew')->name('banner.create-new');
        Route::post('banner/store-new', 'MstBannerController@storeNew')->name('banner.store-new');
        Route::get('banner/edit-new/id/{id}','MstBannerController@editNew')->name('banner.edit-new');
        Route::put('banner/update-new/{id}', 'MstBannerController@updateNew')->name('banner.update-new');

        // Position
        Route::resource('position', 'MstPositionController');
        Route::get('table/position', 'MstPositionController@dataTable')->name('table.position');
        
        /* --------------- ASSET -------------------- */
        Route::resource('asset', 'MstAssetController');
        Route::get('asset/create-new/id/{id}/lg/{lg}', 'MstAssetController@createNew')->name('asset.create-new');
        Route::post('asset/store-new', 'MstAssetController@storeNew')->name('asset.store-new');
        Route::get('asset/edit-new/id/{id}/lg/{lg}', 'MstAssetController@editNew')->name('asset.edit-new');
        Route::put('asset/update-new/{id}', 'MstAssetController@updateNew')->name('asset.update-new');
        Route::get('table/asset', 'MstAssetController@dataTable')->name('table.asset');
        Route::post('asset/fetch-attributes', 'MstAssetController@fetchAttributes')->name('asset.fetch-attributes');
        Route::post('asset/remove-image', 'MstAssetController@removeImage')->name('asset.remove-image');
        Route::post('asset/remove-attr', 'MstAssetController@removeAttr')->name('asset.remove-attr');
        Route::post('asset/set-featured', 'MstAssetController@setFeatured')->name('asset.set-featured');
        Route::get('asset/delete/{id}', 'MstAssetController@delete')->name('asset.delete');
        Route::get('asset/rating-pane/{id}', 'MstAssetController@ratingPane')->name('asset.rating-pane');
        Route::get('asset/asset-pane/{id}', 'MstAssetController@assetPane')->name('asset.asset-pane');
        Route::get('asset/comment-pane/{id}', 'MstAssetController@commentPane')->name('asset.comment-pane');
        Route::get('asset/favorite-pane/{id}', 'MstAssetController@favoritePane')->name('asset.favorite-pane');
        Route::get('asset/investor-pane/{id}', 'MstAssetController@investorPane')->name('asset.investor-pane');
        Route::get('table/asset/investors/{id}', 'MstAssetController@dataInvestor')->name('table.asset.investor');
        // Country
        Route::resource('countries', 'MstCountriesController');
        Route::get('table/countries', 'MstCountriesController@dataTable')->name('table.countries');
        
        /* --------------- REGENCIES -------------------- */
        Route::resource('regencies', 'MstRegenciesController');
        Route::get('table/regencies', 'MstRegenciesController@dataTable')->name('table.regencies');
        Route::get('regencies/delete/{id}', 'MstRegenciesController@delete')->name('regencies.delete');
        Route::get('table/regencies-detail/{id}', 'MstRegenciesController@dataTableDetail')->name('table.regencies-detail');

        // District
        // Route::get('/district','MstDistrictController@index');
        // Route::get('/district/create','MstDistrictController@create');
        // Route::post('/district/post_district','MstDistrictController@store');
        // Route::get('district/edit/{id}','MstDistrictController@edit');
        // Route::post('/district/edit_district/{id}','MstDistrictController@update');
        // Route::get('/district/delete_district/{id}','MstDistrictController@destroy');
        
        /* --------------- DISTRICT -------------------- */
        Route::resource('district','MstDistrictController');
        Route::get('table/district', 'MstDistrictController@dataTable')->name('table.district');
        Route::get('/district/import','MstDistrictController@import');
        Route::post('/district/import_district','MstDistrictController@import_district');
        
        /* --------------- VILLAGE -------------------- */
		Route::resource('village','MstVillageController');
		Route::get('table/village', 'MstVillageController@dataTable')->name('table.village');
        Route::post('search/village', 'MstVillageController@search')->name('village.search');
        Route::get('search-result/village/id/{id}', 'MstVillageController@searchr')->name('village.searchr');
		// Route::get('/master/village','VillageController@index');
		// Route::get('/master/village/create','VillageController@create');
		// Route::post('/master/village/post_village','VillageController@store');
		// Route::get('master/village/edit_village/{id}','VillageController@edit');
		// Route::post('/master/village/edit_village_post/{id}','VillageController@update');
		// Route::get('/master/village/delete_village/{id}','VillageController@destroy');
		// Route::get('/master/village/import','VillageController@import');
		// Route::post('/master/village/import_village','VillageController@import_village');
        // Asset Attribute
        Route::resource('asset-attribute','MstAssetAttributeController');
        Route::get('table/asset-attribute', 'MstAssetAttributeController@dataTable')->name('table.asset-attribute');
        Route::get('table/asset-attribute-detail/{id}', 'MstAssetAttributeController@dataTableDetail')->name('table.asset-attribute-detail');
        Route::get('asset-attribute/delete/{id}', 'MstAssetAttributeController@delete')->name('asset-attribute.delete');
        
        /* --------------- PARTNER -------------------- */
        Route::resource('partner', 'MstPartnerController');
        Route::get('table/partner', 'MstPartnerController@dataTable')->name('table.partner');
        Route::get('partner/delete/{id}', 'MstPartnerController@delete')->name('partner.delete');
        Route::get('partner/create-new/id/{id}/lg/{lg}','MstPartnerController@createNew')->name('partner.create-new');
        Route::post('partner/store-new', 'MstPartnerController@storeNew')->name('partner.store-new');
        Route::get('partner/edit-new/id/{id}','MstPartnerController@editNew')->name('partner.edit-new');
        Route::put('partner/update-new/{id}', 'MstPartnerController@updateNew')->name('partner.update-new');

        /* --------------- GALLERY -------------------- */
        Route::resource('gallery', 'MstGalleryController');
        Route::get('table/gallery', 'MstGalleryController@dataTable')->name('table.gallery');
        Route::post('gallery/set-featured', 'MstGalleryController@setFeatured')->name('gallery.set-featured');
        Route::post('gallery/remove-image', 'MstGalleryController@removeImage')->name('gallery.remove-image');
        Route::get('gallery/delete/{id}', 'MstGalleryController@delete')->name('gallery.delete');
        Route::get('gallery/create-new/id/{id}/lg/{lg}','MstGalleryController@createNew')->name('gallery.create-new');
        Route::get('gallery/edit-new/id/{id}','MstGalleryController@editNew')->name('gallery.edit-new');
        Route::get('gallery/iframe/id/{id}','MstGalleryController@iframeDetail')->name('gallery.iframe');
        Route::post('gallery/store-new', 'MstGalleryController@storeNew')->name('gallery.store-new');
        Route::put('gallery/update-new/{id}', 'MstGalleryController@updateNew')->name('gallery.update-new');
        
        /* --------------- LANGUAGE -------------------- */
        Route::resource('language', 'MstLanguageController');
        Route::get('table/language', 'MstLanguageController@dataTable')->name('table.MstLanguage');

        /* --------------- NEWS CATEGORY -------------------- */
        Route::resource('news-category', 'MstNewsCategoryController');
        Route::get('table/news-category', 'MstNewsCategoryController@dataTable')->name('table.news-category');
        Route::get('table/news-category-detail/{id}', 'MstNewsCategoryController@dataTableDetail')->name('table.news-category-detail');
        Route::get('news-category/delete/{id}', 'MstNewsCategoryController@delete')->name('news-category.delete');

        /* --------------- NEWS TAG -------------------- */
        Route::resource('news-tag', 'MstTagNewsController');
        Route::get('table/news-tag', 'MstTagNewsController@dataTable')->name('table.news-tag');
        Route::get('table/news-tag-detail/{id}', 'MstTagNewsController@dataTableDetail')->name('table.news-tag-detail');
        Route::get('news-tag/delete/{id}', 'MstTagNewsController@delete')->name('news-tag.delete');
        
        /* --------------- NEWS -------------------- */
        Route::resource('news', 'MstNewsController');
        Route::get('table/news', 'MstNewsController@dataTable')->name('table.news');
        Route::get('news/create-new/id/{id}/lg/{lg}','MstNewsController@createNew')->name('news.create-new');
        Route::post('news/store-new', 'MstNewsController@storeNew')->name('news.store-new');
        Route::get('news/edit-new/id/{id}','MstNewsController@editNew')->name('news.edit-new');
        Route::put('news/update-new/{id}', 'MstNewsController@updateNew')->name('news.update-new');
        Route::get('news/delete/{id}', 'MstNewsController@delete')->name('news.delete');
        Route::get('comment-child/{id}', 'MstNewsController@commentChild')->name('news.comment.child');
        Route::get('news/comment/approve/{id}', 'MstNewsController@approveComment')->name('news.comment.approve');
        Route::get('news/comment/reject/{id}', 'MstNewsController@rejectComment')->name('news.comment.reject');

        /* --------------- CONTACT / INBOX -------------------- */
        Route::resource('inbox', 'MstContactUsController');
        Route::get('table/inbox', 'MstContactUsController@dataTable')->name('table.inbox');
        Route::get('inbox/reply/{id}', 'MstContactUsController@reply')->name('inbox.reply');
        
        /* --------------- SECURITY GUIDE -------------------- */
        Route::resource('security-guide', 'SecurityGuideController');
        Route::get('table/security-guide', 'SecurityGuideController@dataTable')->name('table.security-guide');
        Route::get('security-guide/delete/{id}', 'SecurityGuideController@delete')->name('security-guide.delete');
        Route::get('security-guide/show-detail/{id}', 'SecurityGuideController@show_detail')->name('security-guide.show-detail');
        Route::put('security-guide/update-data/{id}', 'SecurityGuideController@updateData')->name('security-guide.update-data');
        Route::get('security-guide/create-new/id/{id}/lg/{lg}','SecurityGuideController@createNew')->name('security-guide.create-new');
        Route::post('security-guide/store-new', 'SecurityGuideController@storeNew')->name('security-guide.store-new');
        Route::get('security-guide/edit-new/id/{id}','SecurityGuideController@editNew')->name('security-guide.edit-new');
        Route::put('security-guide/update-new/{id}', 'SecurityGuideController@updateNew')->name('security-guide.update-new');

        /* --------------- PRIVACY POLICY -------------------- */
        Route::resource('privacy-policy', 'MstPrivacyPolicyController');
        Route::get('table/privacy-policy', 'MstPrivacyPolicyController@dataTable')->name('table.privacy-policy');
        Route::get('privacy-policy/delete/{id}', 'MstPrivacyPolicyController@delete')->name('privacy-policy.delete');
        Route::get('privacy-policy/show-detail/{id}', 'MstPrivacyPolicyController@show_detail')->name('privacy-policy.show-detail');
        Route::put('privacy-policy/update-data/{id}', 'MstPrivacyPolicyController@updateData')->name('privacy-policy.update-data');
        Route::get('privacy-policy/create-new/id/{id}/lg/{lg}','MstPrivacyPolicyController@createNew')->name('privacy-policy.create-new');
        Route::post('privacy-policy/store-new', 'MstPrivacyPolicyController@storeNew')->name('privacy-policy.store-new');
        Route::get('privacy-policy/edit-new/id/{id}','MstPrivacyPolicyController@editNew')->name('privacy-policy.edit-new');
        Route::put('privacy-policy/update-new/{id}', 'MstPrivacyPolicyController@updateNew')->name('privacy-policy.update-new');

        /* --------------- ABOUT US -------------------- */
        Route::resource('about-us', 'MstAboutUsController');
        Route::get('table/about-us', 'MstAboutUsController@dataTable')->name('table.about-us');
        Route::get('about-us/delete/{id}', 'MstAboutUsController@delete')->name('about-us.delete');
        Route::post('about-us/remove-image', 'MstAboutUsController@removeImage')->name('about-us.remove-image');
        Route::get('about-us/show-detail/{id}', 'MstAboutUsController@show_detail')->name('about-us.show-detail');
        Route::put('about-us/update-data/{id}', 'MstAboutUsController@updateData')->name('about-us.update-data');
        Route::get('about-us/create-new/id/{id}/lg/{lg}','MstAboutUsController@createNew')->name('about-us.create-new');
        Route::post('about-us/store-new', 'MstAboutUsController@storeNew')->name('about-us.store-new');
        Route::get('about-us/edit-new/id/{id}','MstAboutUsController@editNew')->name('about-us.edit-new');
        Route::put('about-us/update-new/{id}', 'MstAboutUsController@updateNew')->name('about-us.update-new');

        /* --------------- VOUCHER -------------------- */
        Route::resource('vouchers', 'MstVouchersController');
        Route::get('vouchers/investor/id/{id}','MstVouchersController@investor')->name('vouchers.investor');
        Route::get('table/vouchers', 'MstVouchersController@dataTable')->name('table.vouchers');
        Route::get('table/vouchers-investor/{id}', 'MstVouchersController@dataTableInvestor')->name('table.vouchers.investor');
        Route::get('vouchers/cancel/{id}', 'MstVouchersController@cancel')->name('vouchers.cancel');
        Route::get('vouchers/publish/{id}', 'MstVouchersController@publish')->name('vouchers.publish');
        Route::get('vouchers/show-detail/{id}', 'MstVouchersController@show_detail')->name('vouchers.show-detail');
        Route::put('vouchers/update-data/{id}', 'MstVouchersController@updateData')->name('vouchers.update-data');
        Route::get('vouchers/create-new/id/{id}/lg/{lg}','MstVouchersController@createNew')->name('vouchers.create-new');
        Route::post('vouchers/store-new', 'MstVouchersController@storeNew')->name('vouchers.store-new');
        Route::get('vouchers/edit-new/id/{id}','MstVouchersController@editNew')->name('vouchers.edit-new');
        Route::put('vouchers/update-new/{id}', 'MstVouchersController@updateNew')->name('vouchers.update-new');
        
        /* --------------- USER LOG CODE -------------------- */
        Route::resource('log-code', 'UserLogCodeController');
        Route::get('table/log-code', 'UserLogCodeController@dataTable')->name('table.log-code');
    });

	// Transaction
    Route::group(['prefix'=>'transaction'], function(){
    	/* --------------- INVESTOR -------------------- */
	    Route::resource('investor', 'MstInvestorController');
	    Route::get('table/investor', 'MstInvestorController@dataTable')->name('table.investor');
        Route::get('investor/delete/{id}', 'MstInvestorController@delete')->name('investor.delete');
        Route::get('investor/topup/{id}', 'MstInvestorController@topupform')->name('investor.topupform');
	    Route::get('investor/cashout/{id}', 'MstInvestorController@cashoutform')->name('investor.cashoutform');
        // Investor -> internet banking
	    Route::get('table/internet-banking/{id}', 'InvestorInternetBankingController@dataTable')->name('table.internet-banking');
	    Route::get('internet-banking/create','InvestorInternetBankingController@create')->name('internet-banking.create');
	    Route::post('internet-banking/store', 'InvestorInternetBankingController@store')->name('internet-banking.store');
        Route::get('internet-banking/edit/{id}', 'InvestorInternetBankingController@edit')->name('internet-banking.edit');
        Route::put('internet-banking/update/{id}', 'InvestorInternetBankingController@update')->name('internet-banking.update');
        Route::get('internet-banking/destroy/{id}', 'InvestorInternetBankingController@destroy')->name('internet-banking.destroy');
        // Investor -> atm
	    Route::get('table/atm/{id}', 'InvestorAtmController@dataTable')->name('table.atm');
	    Route::get('atm/create','InvestorAtmController@create')->name('atm.create');
	    Route::post('atm/store', 'InvestorAtmController@store')->name('atm.store');
        Route::get('atm/edit/{id}', 'InvestorAtmController@edit')->name('atm.edit');
        Route::put('atm/update/{id}', 'InvestorAtmController@update')->name('atm.update');
        Route::get('atm/destroy/{id}', 'InvestorAtmController@destroy')->name('atm.destroy');
        /* --------------- INVESTOR / BALANCE -------------------- */
	    Route::get('balance/balance-pane', 'InvestorBalanceController@pane')->name('balance.pane');
	    Route::get('table/balance/{id}', 'InvestorBalanceController@dataTable')->name('table.balance');
        Route::get('balance/get/{id}', 'InvestorBalanceController@getBalance')->name('balance.get');
        Route::get('balance/topup-form/{id}', 'InvestorBalanceController@topupForm')->name('balance.topup-form');
        Route::post('balance/topup', 'InvestorBalanceController@topup')->name('balance.topup');
        Route::get('balance/cashout-form/{id}', 'InvestorBalanceController@cashoutForm')->name('balance.cashout-form');
        // Investor -> transaction
	    Route::get('transaction/transaction-pane', 'InvestorTransactionController@pane')->name('transaction.pane');
	    Route::get('table/transaction/{id}', 'InvestorTransactionController@dataTable')->name('table.transaction');
        // Investor -> asset
        Route::get('investor-asset/asset-pane', 'InvestorAssetController@assetPane')->name('investor-asset.pane');
	    Route::get('table/investor-asset/{id}', 'InvestorAssetController@assetDataTable')->name('table.investor-asset');
        Route::get('investor-asset/detail/{id}', 'InvestorAssetController@assetDetail')->name('investor-asset.detail');
        // Investor -> asset fav
        Route::get('investor-asset/fav-asset-pane', 'InvestorAssetController@assetFavPane')->name('investor-asset-fav.pane');
	    Route::get('table/investor-asset-fav/{id}', 'InvestorAssetController@assetFavDataTable')->name('table.investor-asset-fav');

        /* --------------- MONITORING TOPUP -------------------- */
        Route::resource('monitoring-topup', 'MonitoringtopupController');
        Route::get('table/monitoring-topup', 'MonitoringtopupController@dataTable')->name('table.MonitoringTopup');
	    Route::get('table/monitoring-topup-history/{id}', 'MonitoringtopupController@dataHistory')->name('table.topup.history');
        Route::get('monitoring-topup/verify/{id}', 'MonitoringtopupController@verify')->name('monitoring-topup.verify');
        Route::post('monitoring-topup/reject', 'MonitoringtopupController@reject')->name('monitoring-topup.reject');
        Route::get('widget/monitoring-topup', 'MonitoringtopupController@reloadWidget')->name('monitoring-topup.widget');
        Route::post('monitoring-topup/recheck', 'MonitoringtopupController@recheck')->name('monitoring-topup.recheck');

        /* --------------- MONITORING CASH OUT -------------------- */
        Route::resource('monitoring-cashout', 'MonitoringTopupOutController');
        Route::get('table/monitoring-cashout', 'MonitoringTopupOutController@dataTable')->name('table.MonitoringTopupOut');
        Route::get('table/monitoring-cashout-history/{id}', 'MonitoringTopupOutController@dataHistory')->name('table.cashout.history');
        Route::get('monitoring-cashout/process/{id}', 'MonitoringTopupOutController@process')->name('monitoring-cashout.process');
        Route::get('monitoring-cashout/verify/{id}', 'MonitoringTopupOutController@verify')->name('monitoring-cashout.verify');
        Route::post('monitoring-cashout/reject', 'MonitoringTopupOutController@reject')->name('monitoring-cashout.reject');
        Route::get('widget/monitoring-cashout', 'MonitoringTopupOutController@reloadWidget')->name('monitoring-cashout.widget');
        
        /* --------------- OFFERING PURCHASE -------------------- */
        Route::resource('offering-purchase', 'OfferingPurchaseController');
        Route::get('table/offering-purchase', 'OfferingPurchaseController@dataTable')->name('table.offering-purchase');
        Route::get('offering-purchase/reply/{id}', 'OfferingPurchaseController@reply')->name('offering-purchase.reply');
    });
        
    // Setting
    Route::group(['prefix'=>'setting'], function(){
       // Module
        Route::resource('menu', 'SecModuleController');
        Route::get('table/menu', 'SecModuleController@dataTable')->name('table.SecModule');
        //Access Level
		Route::resource('access-level', 'SecAccessLevelController');
		Route::get('table/access-level', 'SecAccessLevelController@dataTable')->name('table.access-level'); 
		Route::get('table/detail-access-level', 'SecAccessLevelController@dataTableDetail')->name('table.detail-access-level');
		Route::post('access-level/add-act', 'SecAccessLevelController@addAct')->name('access-level.add-act'); 
		Route::post('access-level/remove-act', 'SecAccessLevelController@removeAct')->name('access-level.remove-act'); 
    });
	
	// Route::get('/master/country','CountryController@index');
	// Route::get('/master/country/create','CountryController@create');
	// Route::post('/master/country/post_country','CountryController@store');
	// Route::get('/master/country/edit/{id}','CountryController@edit');
	// Route::post('/master/country/edit_country/{id}','CountryController@update');
	// Route::get('/master/country/import','CountryController@import');
	// Route::post('/master/country/proses_import','CountryController@proses_import');

	Route::get('/master/province','ProvinceController@index');
	Route::get('/master/province/create','ProvinceController@create');
	Route::post('/master/province/post_province','ProvinceController@store');
	Route::post('/master/province/edit_province/{id}','ProvinceController@update');
	Route::get('/master/province/delete/{id}','ProvinceController@destroy');
	Route::get('/master/province/edit/{id}','ProvinceController@edit');
	Route::get('/master/province/delete/{id}','ProvinceController@destroy');
	Route::get('/master/province/import','ProvinceController@import');
	Route::post('/master/province/import_province','ProvinceController@import_province');
	
	/*Route::get('/master/regency','RegencyController@index');
	Route::get('/master/regency/create','RegencyController@create');
	Route::post('/master/regency/post_regency','RegencyController@store');
	Route::get('/master/regency/edit_regency/{id}','RegencyController@edit');
	Route::post('/master/regency/regency_edit/{id}','RegencyController@update');
	Route::get('/master/regency/delete_regency/{id}','RegencyController@destroy');
	Route::post('/master/regency/regency_edit/{id}','RegencyController@update');
	Route::get('/master/regency/import','RegencyController@import');
	Route::post('/master/regency/import_post_regency','RegencyController@import_regency');*/

	
	Route::get('/master/data_address','MstAddressController@index');
	Route::get('/master/address/create','MstAddressController@create');
	Route::get('/master/address/province/{id}','MstAddressController@province');
	Route::get('/master/address/regency/{id}','MstAddressController@regency');
	Route::get('/master/address/district/{id}','MstAddressController@district');
	Route::get('/master/address/village/{id}','MstAddressController@village');
	Route::post('/master/address/post_address','MstAddressController@store');

	/* --------------- MONITORING CASH OUT -------------------- */
	// Route::resource('master/category-news', 'MstNewsCategoryController');
	// Route::get('table/category-news', 'MstNewsCategoryController@dataTable')->name('table.BlgCategoryNews');
	// Route::resource('master/category-news', 'BlgCategoryNewsController');
	// Route::get('table/category-news', 'BlgCategoryNewsController@dataTable')->name('table.BlgCategoryNews');
	
	// Blg News
	// Route::resource('master/news', 'BlgNewsController');
	// Route::get('table/news', 'BlgNewsController@dataTable')->name('table.BlgNews');
	// Route::get('news/setposition/{id}','BlgNewsController@setposition')->name('banner/bannerposition');
	// Route::get('news/delete/{id}', 'BlgNewsController@delete')->name('news.delete');
	
	

	//Contact Us
	// Route::resource('master/contact-us', 'MstContactUsController');
	// Route::get('table/contact-us', 'MstContactUsController@dataTable')->name('table.MstContactUs');
	
	//Tag News
	// Route::resource('master/tag-news', 'MstTagNewsController');
	// Route::get('table/tag-news', 'MstTagNewsController@dataTable')->name('table.MstTagNews');

	Route::resource('master/class-price', 'MstClassPriceController');
	Route::get('table/class-price', 'MstClassPriceController@dataTable')->name('table.MstClassPrice');

	

	// Route::resource('master/security-guide', 'SecurityGuideController');
	// Route::get('table/security-guide', 'SecurityGuideController@dataTable')->name('table.SecurityGuide');
	// Route::get('security-guide/delete/{id}', 'SecurityGuideController@delete')->name('security-guide.delete');
 //    Route::get('security-guide/show-detail/{id}', 'SecurityGuideController@show_detail')->name('security-guide.show-detail');
 //    Route::put('security-guide/update-data/{id}', 'SecurityGuideController@updateData')->name('security-guide.update-data');

	// Route::resource('master/partner', 'MstPartnerController');
	// Route::get('table/partner', 'MstPartnerController@dataTable')->name('table.MstPartner');
	// Route::get('partner/delete/{id}', 'MstPartnerController@delete')->name('partner.delete');

	Route::resource('master/voucher', 'MstVoucherController');
	Route::get('table/voucher', 'MstVoucherController@dataTable')->name('table.MstVoucher');
	Route::get('voucher/delete/{id}', 'MstVoucherController@delete')->name('voucher.delete');
    Route::get('voucher/detail/{id}', 'MstVoucherController@detail')->name('voucher.detail');
    Route::get('voucher/edit_detail/{id}', 'MstVoucherController@edit_detail')->name('voucher.edit_detail');
    Route::get('voucher/show-detail/{id}', 'MstVoucherController@show_detail')->name('voucher.show-detail');
    Route::put('voucher/update-satu/{id}', 'MstVoucherController@update_satu')->name('voucher.update-satu');

});





// Route::get('/home', 'HomeController@index')->name('home');
