<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use DB;

class MstMemberDescriptionCreateRule implements Rule
{
    public $code;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($c)
    {
        $this->code = $c;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $result = true;
        $model = DB::select("
            select 
                ml.*
            from 
                mst_member as m,
                mst_member_lang as ml 
            where 
                ml.member_id = m.id 
            and lower(ml.description) = lower('".$value."')
            and ml.code = '".$this->code."'
            and m.is_deleted = 0
        ");
        // dd($value.' - '.$this->code);

        if($model != null)
            $result = false;

        return $result;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The name has already been taken';
    }
}
