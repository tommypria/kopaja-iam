<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MstInvestor;
use App\Models\MstBank;
use App\Models\MstCurrency;
use App\Models\MstCountries;
use App\Models\TrcSaldo;
use App\Models\TrcInvBank;
use App\Models\TrcTransactionSaldo;
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use DataTables;
use DB;
use App\Utility;

class MstInvestorController extends Controller
{
    const MODULE_NAME = 'Investor Detail';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!$this->checkAccess(self::MODULE_NAME, 'R'))
            abort(401, 'Unauthorized action.');

        return view('transaction.investor.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!$this->checkAccess(self::MODULE_NAME, 'C'))
            abort(401, 'Unauthorized action.');

        $model = new MstInvestor();

        $countries = DB::table('mst_countries')->get();

        $bank = MstBank::where('active','1')
                    ->where('is_deleted','0')
                    ->where('card_type','D')
                    ->pluck('name', 'id')
                    ->all();

        $currency = MstCurrency::pluck('currency','code')->all();

        return view('transaction.investor.create', compact(['model','countries','bank','currency']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$this->checkAccess(self::MODULE_NAME, 'C'))
            abort(401, 'Unauthorized action.');

        $validate = $this->validate($request, [
            'full_name'     => 'required',
            'username'      => 'required|string|unique:mst_investor,username,1,is_deleted',
            'password'      => 'required|required_with:password_confirmation|string|confirmed',
            'gender'        => 'required',
            'birth_place'   => 'required|string',
            'birth_date'    => 'required',
            'address'       => 'required',
            'country'       => 'required',
            'zip_code'       => 'required',
            // 'province'       => 'required',
            // 'regency'       => 'required',
            // 'district'       => 'required',
            // 'villages_id'       => 'required',
            'email'       => 'required|email',
            'phone'       => 'required|string',
            'photo'       => 'required|file|image|mimes:jpg,jpeg,png|max:5048',
            'ktp_photo'       => 'required|file|image|mimes:jpg,jpeg,png|max:5048',
            'npwp_photo'       => 'required|file|image|mimes:jpg,jpeg,png|max:5048',
            'ktp_number'       => 'required|string',
            'npwp_number'       => 'required|string',
            'bank_id'       => 'required',
            'acc_name'       => 'required',
            'acc_number'       => 'required',
            'currency_code'       => 'required'
        ]);

        if($validate)
        {
            $userId = Auth::user()->id;
            $active = 1;
            $current_date = date('Y-m-d H:i:s');
            
            //Photo Investor
            $image_inv = $request->file('photo');
            $filename_inv = time().$image_inv->getClientOriginalName();
            $path = base_path().'/public/images/investor/';
            $image_inv->move($path, $filename_inv);
            
            //Photo KTP
            $image_ktp = $request->file('ktp_photo');
            $filename_ktp = time().$image_ktp->getClientOriginalName();
            $path_ktp = base_path().'/public/images/investor/foto_berkas/';
            $image_ktp->move($path_ktp, $filename_ktp);
            
            //Photo NPWP
            $image_npwp = $request->file('npwp_photo');
            $filename_npwp = time().$image_npwp->getClientOriginalName();
            $path_npwp = base_path().'/public/images/investor/foto_berkas/';
            $image_npwp->move($path_npwp, $filename_npwp);

            $data = [
                'username' => $request->username,
                'password' => sha1($request->password),
                'full_name' => $request->full_name,
                'gender' => $request->gender,
                'birth_date' => date('Y-m-d', strtotime($request->birth_date)),
                'birth_place' => $request->birth_place,
                'address' => $request->address,
                'zip_code' => $request->zip_code,
                'email' => $request->email,
                'phone' => $request->phone,
                'photo' => $filename_inv,
                'ktp_number' => $request->ktp_number,
                'ktp_photo' => $filename_ktp,
                'npwp_number' => $request->npwp_number,
                'npwp_photo' => $filename_npwp,
                'currency_code' => $request->currency_code,
                'countries_id' => $request->country,
                'active' => $active,
                'created_at_emp' => $current_date,
                'created_by_emp' => $userId,
                'updated_at_emp' => $current_date,
                'updated_by_emp' => $userId
            ];
            
            if($model = MstInvestor::create($data))
            {
                // bank account
                $bank = [
                    'investor_id' => $model->id,
                    'bank_id' => $request->bank_id,
                    'account_holder_name' => $request->acc_name,
                    'account_number' => $request->acc_number,
                    'active' => '1'
                    // 'payment_methode'   => '2',
                ];

                if(TrcInvBank::create($bank))
                {
                    \UserLogActivity::addLog('Create '.self::MODULE_NAME.' ID #'.$model->id.' Successfully');
                    return redirect('transaction/investor/'. base64_encode($model->id))->with('success','Investor successfully created');
                }
            }
            else
                Redirect::back()->withErrors(['error', 'Failed']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!$this->checkAccess(self::MODULE_NAME, 'S'))
            abort(401, 'Unauthorized action.');

        $id = base64_decode($id);

        $model = MstInvestor::findOrFail($id);

        $countries = MstCountries::pluck('name','id')->all();

        $currency = MstCurrency::pluck('currency','code')->all();

        $bank_ib = MstBank::where('active', 1)
                    ->where('is_deleted', 0)
                    ->where('card_type', 'C')
                    ->pluck('name', 'id')
                    ->all();

        $bank = MstBank::where('active','1')
                    ->where('is_deleted','0')
                    ->where('card_type','D')
                    ->pluck('name', 'id')
                    ->all();

        $invbank = TrcInvBank::where('investor_id',$id)
                    ->where('active','1')
                    ->where('is_deleted','0')
                    ->orderBy('id')
                    ->first();

        $trcinvbank = new TrcInvBank();
        
        return view('transaction.investor.detail', compact([
            'model',
            'countries',
            'bank',
            'countries', 
            'currency', 
            // 'provinces', 
            // 'regencies', 
            // 'districts', 
            // 'villages', 
            // 'uti', 
            // 'address',
            'invbank',
            'trcinvbank',
            'bank_ib'
        ]));
    }

    public function profilePane($id)
    {
        $model = MstInvestor::findOrFail($id);

        $countries = DB::table('mst_countries')->get();

        $bank = MstBank::where('active','1')
                    ->where('is_deleted','0')
                    ->where('card_type','D')
                    ->pluck('name', 'id')
                    ->all();

        $uti = new Utility();
        $address = $uti->getDetailAddress($model->villages_id);
        $countries = DB::table('mst_countries')->get();
        $provinces = DB::table('mst_provinces')->where('countries_id',$address['country_id'])->get();
        $regencies = DB::table('mst_regencies')->where('provinces_id',$address['province_id'])->get();
        $districts = DB::table('mst_districts')->where('regencies_id',$address['regency_id'])->get();
        $villages = DB::table('mst_villages')->where('districts_id',$address['district_id'])->get();

        $invbank = TrcInvBank::where('investor_id',$id)
                    ->where('active','1')
                    ->where('is_deleted','0')
                    ->orderBy('id')
                    ->first();

        return view('transaction.investor.tab.profile', compact([
            'model',
            'countries',
            'bank',
            'countries', 
            'provinces', 
            'regencies', 
            'districts', 
            'villages', 
            'uti', 
            'address',
            'invbank'
        ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!$this->checkAccess(self::MODULE_NAME, 'U'))
            abort(401, 'Unauthorized action.');

        $a = ['mst_contact','mst_notification','trc_balance','trc_cart','trc_inv_bank'];
        $b = ['investor_id','investor_id','investor_id','investor_id','investor_id'];


        if(!$this->isAllowedToUpdate($a, $b, $id))
            abort(401, 'Table has related.');

        $model = MstInvestor::findOrFail($id);
        $uti = new Utility();
        $address = $uti->getDetailAddress($model->village_id);
        $countries = DB::table('mst_countries')->get();
        $provinces = DB::table('mst_provinces')->where('countries_id',$address['country_id'])->get();
        $regencies = DB::table('mst_regencies')->where('provinces_id',$address['province_id'])->get();
        $districts = DB::table('mst_districts')->where('regencies_id',$address['regency_id'])->get();
        $villages = DB::table('mst_villages')->where('districts_id',$address['district_id'])->get();
        

         $page = MstMember::where('active','1')
                    ->where('is_deleted','0')
                    ->pluck('member', 'id')
                    ->all();

        return view('transaction.investor.update', 
                compact(['model', 
                        'countries', 
                        'provinces', 
                        'regencies', 
                        'districts', 
                        'villages', 
                        'uti', 
                        'address','page']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!$this->checkAccess(self::MODULE_NAME, 'U'))
            abort(401, 'Unauthorized action.');

        $id = base64_decode($id);

        $validate = $this->validate($request, [
            // 'member_id'     => 'required',
            'username' => 'required|string|unique:mst_investor,username,'.$id.',id,is_deleted,0',
            'password' => 'nullable|required_with:password_confirmation|string|confirmed',
            'full_name' => 'required',
            'gender' => 'required',
            'birth_place' => 'required|string',
            'birth_date' => 'required',
            'countries_id' => 'required',
            'zip_code' => 'required',
            'address' => 'required',
            'email' => 'required|string',
            'phone' => 'required|string',
            'ktp_number' => 'required|string',
            'npwp_number' => 'required|string',
            'active' => 'required',
            'currency_code' => 'required'
            // 'province'       => 'required',
            // 'regency'       => 'required',
            // 'district'       => 'required',
            // 'villages_id'       => 'required',

            // 'bank_id'       => 'required',
            // 'acc_name'       => 'required',
            // 'acc_number'       => 'required'

        ]);

        if($validate)
        {
            $userId = Auth::user()->id;
            $current_date = date('Y-m-d H:i:s');
            $old_image_inv = '';
            $old_image_ktp = '';
            $old_image_npwp = '';
            $old_password = '';


            $model = MstInvestor::findOrFail($id);
            
            $data = [
                'username'  => $request->username,
                'full_name' => $request->full_name,
                'gender'  => $request->gender,
                'birth_date'  => date('Y-m-d', strtotime($request->birth_date)),
                'birth_place'  => $request->birth_place,
                'address'  => $request->address,
                'countries_id'  => $request->countries_id,
                'zip_code'  => $request->zip_code,
                'email'  => $request->email,
                'phone'  => $request->phone,
                'ktp_number'  => $request->ktp_number,
                'npwp_number'  => $request->npwp_number,
                'currency_code'     => $request->currency_code,
                'active'    => $request->active,
                'updated_at_emp'=> $current_date,
                'updated_by_emp'=> $userId
            ];
            
            $image_inv = $request->file('photo');
            if($image_inv != null)
            {
                $filename_inv = time().$image_inv->getClientOriginalName();
                $path = base_path().'/public/images/investor/';
                $image_inv->move($path, $filename_inv);

                $data = array_merge($data, ['photo'=>$filename_inv]);

                if($model->photo != null)
                    $old_image_inv = $model->photo;
            }
            
            $image_ktp = $request->file('ktp_photo');
            if($image_ktp != null)
            {
                $filename_ktp = time().$image_ktp->getClientOriginalName();
                $path_ktp = base_path().'/public/images/investor/foto_berkas/';
                $image_ktp->move($path_ktp, $filename_ktp);

                $data = array_merge($data, ['ktp_photo'=>$filename_ktp]);

                if($model->ktp_photo != null)
                    $old_image_ktp = $model->ktp_photo;
            }

            $image_npwp = $request->file('npwp_photo');
            if($image_npwp != null)
            {
                
                $filename_npwp = time().$image_npwp->getClientOriginalName();
                $path_npwp = base_path().'/public/images/investor/foto_berkas/';
                $image_npwp->move($path_npwp, $filename_npwp);

                $data = array_merge($data, ['npwp_photo'=>$filename_npwp]);

                if($model->npwp_photo != null)
                    $old_image_npwp = $model->npwp_photo;
            }
            
            $password1 = $request->password;
            if($password1 != null)
            {
                $data = array_merge($data, ['password'  => sha1($request->password)]);
            }

            if($model->update($data))
            {
                if($old_image_inv != null)
                    unlink(base_path().'/public/images/investor/'.$old_image_inv);

                if($old_image_ktp != null)
                    unlink(base_path().'/public/images/investor/foto_berkas/'.$old_image_ktp);

                if($old_image_npwp != null)
                    unlink(base_path().'/public/images/investor/foto_berkas/'.$old_image_npwp);
                
                \UserLogActivity::addLog('Update '.self::MODULE_NAME.' ID #'.$model->id.' Successfully');
                return redirect('transaction/investor/'. base64_encode($model->id))->with('success','Investor data updated');
            }
            else
                Redirect::back()->withErrors(['error', 'Failed']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!$this->checkAccess(self::MODULE_NAME, 'D'))
            abort(401, 'Unauthorized action.');

        $deleted_date   = date('Y-m-d H:i:s');
        $deleted_by     = Auth::user()->id;

        DB::update("update mst_investor set 
            deleted_at_emp='".$deleted_date."',
            deleted_by_emp = ".$deleted_by.",
            is_deleted = 1
            where id = ".$id."");

        \UserLogActivity::addLog('Delete '.self::MODULE_NAME.' ID #'.$id.' Successfully');
    }


    public function delete($id)
    {
        if(!$this->checkAccess(self::MODULE_NAME, 'D'))
            abort(401, 'Unauthorized action.');

        $deleted_date   = date('Y-m-d H:i:s');
        $deleted_by     = Auth::user()->id;

        $delete = DB::update("update mst_investor set 
            deleted_at = '".$deleted_date."',
            deleted_by = ".$deleted_by.",
            is_deleted = 1
            where id = ".$id."");

        if($delete)
        {
            \UserLogActivity::addLog('Delete '.self::MODULE_NAME.' ID #'.$id.' Successfully');
            return redirect('transaction/investor')->with('success', 'Deleted');
        }
        else
            return redirect('transaction/investor/show/'.$id)->with('error', 'Failed');
    }

    public function dataTable()
    {
        // $model = DB::select("
        //     SELECT 
        //         i.id as inv_id, i.full_name, ml.description, i.active
        //     FROM
        //         mst_investor AS i
        //             JOIN
        //         mst_member AS m ON i.member_id = m.id
        //             JOIN
        //         mst_member_lang AS ml ON m.id = ml.member_id
        //     WHERE
        //         ml.code = 'IND'
        //     ORDER BY i.id DESC
        // ");

        $model = MstInvestor::where('is_deleted', 0)
                ->orderBy('id','desc');
        
        return DataTables::of($model)
            // ->addColumn('checkbox', '<input type="checkbox" id="'.$model->sec_role_id.'" name="checkbox">' )
            ->addColumn('action', function($model){
                return view('transaction.investor.action', [
                    'model' => $model,
                    'url_show'=> route('investor.show', base64_encode($model->id) ),
                    'url_destroy'=> route('investor.destroy', base64_encode($model->id) )
                ]);
            })
            ->editColumn('active', function($model){
                return $model->active == 1 ? 'Active' : 'Inactive';
            })
            /*->editColumn('created_at_emp', function($model){
                $date = '';
                if($model->created_at_emp)
                    $date = date('d-m-Y H:i:s', strtotime($model->created_at_emp));

                return $date;
            })
            ->editColumn('created_by_emp', function($model){
                $uti = new utility();
                $user = '';
                if($model->created_by_emp)
                    $user = $uti->getUser($model->created_by_emp);

                return $user;
            })*/
            // ->addIndexColumn()
            ->rawColumns(['action', 'active'])
            ->make(true);
    }

    public function topupform($id)
    {
        return view('transaction.investor.topup', compact($id));
    }


}
