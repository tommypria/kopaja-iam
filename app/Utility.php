<?php
namespace App;

use DB;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Models\MstEmployee;
use App\Models\MstInvestor;

class Utility
{
	public function getUser($id)
    {
    	$fullname = '';
    	$model = DB::table('mst_employee')
                        ->where('id','=',$id)
                        ->first();
    	if($model)
    		$fullname = $model->full_name;

    	return $fullname;
    }

    public function localDate($date)
    {
        return date('d M Y H:i', strtotime($date)).' WIB';
    }

    public function getDetailAddress($id) //village_id
    {
        $data = DB::table('mst_villages as v')
                ->join('mst_districts as d','v.districts_id','=','d.id')
                ->join('mst_regencies as r','d.regencies_id','=','r.id')
                ->join('mst_regencies_lang as rl','rl.regencies_id','=','r.id')
                ->join('mst_provinces as p','r.provinces_id','=','p.id')
                ->join('mst_countries as c','p.countries_id','=','c.id')
                ->select('v.name as village', 
                        'd.name as district', 
                        'rl.name as regency', 
                        'p.name as province', 
                        'c.name as country',
                        'v.id as vid',
                        'd.id as did',
                        'r.id as rid',
                        'p.id as pid',
                        'c.id as cid'
                )
                ->where('v.id',$id)
                ->where('rl.code',"IND")
                ->first();
        $village = $data->village;
        $district = $data->district;
        $regency = $data->regency;
        $province = $data->province;
        $country = $data->country;
        return [
            'village'  => $village,
            'district'  => $district,
            'regency'  => $regency,
            'province'  => $province,
            'country'  => $country,
            'village_id'  => $data->vid,
            'district_id'  => $data->did,
            'regency_id'  => $data->rid,
            'province_id'  => $data->pid,
            'country_id'  => $data->cid
        ];
    }

    public function paymentMethod()
    {
        return [
            '1' => 'Internet Banking',
            '2' => 'Manual Transfer',
            '3' => 'Virtual Account',
            '4' => 'Indomaret'
        ];
    }

    public function getPaymentMethod($id)
    {
        $value = '';
        foreach($this->paymentMethod() as $key => $value)
        {
            if($id == $key)
            {
                $value = $value;
                break;
            }
        }
        return $value;
    }

    public function listMenu()
    {
        $id = Auth::user()->role_id;

        $menus = '';
        // get menu level 1
        $parents = DB::table('sec_accesslevel as a')
                ->join('sec_module as b','a.module_id','=','b.id')
                ->where('a.role_id',$id)
                ->where('b.parent_id',null)
                ->select('a.module_id as id','b.module','b.link','b.icon')
                ->groupBy('a.module_id','b.module')
                ->orderBy('b.sort')
                ->get();
        // dd($id);
        // dd($parents);
        foreach ($parents as $first) 
        {
            $icon = $first->icon == '' ? 'fa fa-folder' : $first->icon;
            // cek if has child
            $childs = $this->getChild($first->id);
                    // dd($childs);
            
            if(!$childs)
            {
                $menus .= '<li>
                    <a href="'.$first->link.'">
                        <i class="'.$icon.'"></i>
                        <span>'.$first->module.'</span>
                    </a>
                </li>';
            }
            else
            {
                $menus .= '<li class="treeview">
                    <a href="#">
                        <i class="'.$icon.'"></i>
                        <span>'.$first->module.'</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>';
                $menus .= '<ul class="treeview-menu">';
                foreach ($childs as $second) {
                    $icon = $second->icon == '' ? 'fa fa-circle-o' : $second->icon;
                    $grandchilds = $this->getChild($second->id);
                    if(!$grandchilds)
                    {
                        $menus .= '<li>
                            <a href="'.$second->link.'">
                                <i class="'.$icon.'"></i>
                                <span>'.$second->module.'</span>
                            </a>
                        </li>';
                    }
                    else
                    {
                        $menus .= '<li class="treeview">
                            <a href="#">
                                <i class="'.$icon.'"></i>
                                <span>'.$second->module.'</span>
                                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                            </a>';
                        $menus .= '<ul class="treeview-menu">';
                        foreach($grandchilds as $third)
                        {
                            $icon = $third->icon == '' ? 'fa fa-circle-o' : $third->icon;
                            $menus .= '<li>
                                <a href="'.$third->link.'">
                                    <i class="'.$icon.'"></i>
                                    <span>'.$third->module.'</span>
                                </a>
                            </li>';
                        }
                        $menus .= '</ul></li>';
                    }
                }
                $menus .= '</ul></li>';
            }
        }

        echo $menus;
    }

    private function getChild($id) // parent_id
    {
        return DB::table('sec_module as a')
                ->join('sec_accesslevel as b','a.id','=','b.module_id')
                ->where('a.parent_id', $id)
                ->where('a.active','1')
                ->where('a.is_deleted', '0')
                ->where('b.role_id', Auth::user()->role_id)
                ->select('a.id','a.module','a.parent_id','a.link','a.icon')
                ->groupBy('b.module_id','a.module')
                ->orderBy('a.sort')
                ->get()
                ->toArray();
    }

    public function bestSellingAsset($year, $month)
    {
        $rawdata = [];

        $query = "
        select 
            ai.asset_id as id,
            al.asset_name,
            count(ta.investor_id) as total
        from
            trc_transaction_asset as ta
        left join trc_asset_investor as ai on ta.id = ai.trc_asset_id
        join mst_asset as a on ai.asset_id = a.id
        join mst_asset_lang as al on a.id = al.asset_id
        where 
            al.code = 'IND'
        and lower(ai.status) = 'active' ";

        if($year != '' && $month != '')
        {
            $query .= "and date_format(ta.date, '%Y') = ? ";
            $query .= "and date_format(ta.date, '%m') = ? ";
            $query .= "group by 
                            ai.asset_id,
                            al.asset_name
                    order by 
                            count(ta.investor_id) desc
                    limit 10;";
            $rawdata = DB::select($query, [$year, $month]);
        }
        elseif($year == '' && $month == '')
        {
            $query .= "group by 
                            ai.asset_id,
                            al.asset_name
                    order by 
                            count(ta.investor_id) desc
                    limit 10;";
            $rawdata = DB::select($query);
        }
        elseif($year != '')
        {
            $query .= "and date_format(ta.date, '%Y') = ? ";
            $query .= "group by 
                                ai.asset_id,
                                al.asset_name
                        order by 
                                count(ta.investor_id) desc
                        limit 10; ";
            $rawdata = DB::select($query, [$year]);   
        }

        return $rawdata;
    }

    public function highestInvestment($year, $month)
    {
        $rawdata = [];

        $query = "
            select 
                ta.investor_id as id,
                i.full_name,
                sum(ta.total_amount) as amount
            from 
            trc_transaction_asset as ta 
            left join mst_investor as i on ta.investor_id = i.id 
            where 
                lower(ta.status)='paid'
            and lower(ta.verified)='y'
        ";

        if($year != '' && $month != '')
        {
            $query .= "and date_format(ta.date, '%Y') = ? ";
            $query .= "and date_format(ta.date, '%m') = ? ";
            $query .= "group by 
                        ta.investor_id,
                        i.full_name
                    order by 
                        sum(ta.total_amount) desc
                    limit 10;";
            $rawdata = DB::select($query, [$year, $month]);
        }
        elseif($year == '' && $month == '')
        {
            $query .= "group by 
                        ta.investor_id,
                        i.full_name
                    order by 
                        sum(ta.total_amount) desc
                    limit 10;";
            $rawdata = DB::select($query);   
        }
        elseif($year != '')
        {
            $query .= "and date_format(ta.date, '%Y') = ? ";
            $query .= "group by 
                            ta.investor_id,
                            i.full_name
                        order by 
                            sum(ta.total_amount) desc
                        limit 10; ";
            $rawdata = DB::select($query, [$year]);   
        }

        return $rawdata;
    }

    /*
    example : 
        $childTables = [trc_inv_bank]
        $fk_ids = [bank_id]
        $pk_id = 1
    */
    public function isAllowedToUpdate($childTables = array(), $fk_ids = array(), $pk_id)
    {
        // looping pengecekan data di child table
        $no = 0;
        $total = 0;
        foreach($childTables as $table)
        {
            $count = DB::table($table)
                    ->where($fk_ids[$no], $pk_id)
                    ->where('is_deleted', '0')
                    ->count();
            $total = $total + $count;
        }

        return $total > 0 ? false : true;
    }

    public function isAllowedToUpdate2($childTables = array(), $fk_ids = array(), $pk_id)
    {
        // looping pengecekan data di child table
        $no = 0;
        $total = 0;
        foreach($childTables as $table)
        {
            $count = DB::table($table)
                    ->where($fk_ids[$no], $pk_id)
                    ->count();
            $total = $total + $count;
        }

        return $total > 0 ? false : true;
    }

    public function USDIDRRate()
    {
        $client = new Client();
        $result = $client->get('http://apilayer.net/api/live?access_key=feb247b947ca8681726a7746766fec80&currencies=IDR&source=USD&format=1');
        $result = json_decode((string)$result->getBody());
        $rate = $result->quotes->USDIDR;
        return $rate;
    }

    public function investor($id)
    {
        $model = MstInvestor::find($id);
        return $model->full_name;
    }

    public function employee($id)
    {
        $model = MstEmployee::find($id);
        return $model->full_name;
    }

    public function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }
}
?>