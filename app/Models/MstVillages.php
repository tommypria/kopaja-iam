<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstVillages extends Model
{
    protected $table = 'mst_villages';

    protected $fillable = [
    	'id',
    	'districts_id',
    	'name'
    ];

    public $timestamps = false;

    public $incrementing = false;

    protected $keyType = 'string';
}
