<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstInvestor extends Model
{
	public $timestamps = false;
    protected $table = 'mst_investor';
    protected $fillable = [
    	'member_id',
    	'username',
    	'password',
    	'full_name',
    	'gender',
    	'birth_date',
    	'birth_place',
    	'address',
    	'villages_id',
    	'zip_code',
    	'email',
    	'phone',
    	'photo',
    	'ip_address',
    	'token_phone',
    	'remember_token',
    	'register_on',
    	'ktp_number',
    	'ktp_photo',
    	'npwp_number',
    	'npwp_photo',
        'currency',
    	'activation_code',
    	'forgot_pass_code',
    	'forgot_pass_time',
    	'active',
    	'created_at_investor',
    	'created_by_emp',
    	'created_at_emp',
    	'created_at_investor',
    	'updated_at_emp',
    	'updated_by_emp',
    	'deleted_at_emp',
    	'deleted_by_emp',
    	'is_deleted'
    ];
}
