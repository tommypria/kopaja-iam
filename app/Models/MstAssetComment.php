<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstAssetComment extends Model
{
    protected $table = 'mst_asset_comment';

    protected $fillable = [
    	'asset_id',
    	'comment',
    	'active',
    	'created_by',
    	'updated_by',
    	'deleted_by',
    	'deleted_at',
    	'is_deleted'
    ];
}
