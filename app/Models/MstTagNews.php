<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstTagNews extends Model
{
    protected $table = 'blg_tag_news';
    protected $fillable = ['blg_news_id','desc','active','created_by','updated_by','deleted_date','deleted_by','is_deleted'];
}
