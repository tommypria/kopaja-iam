<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrcBalance extends Model
{
	public $timestamps = false;
	
    protected $table = "trc_balance";

    protected $fillable = [
    	'investor_id',
    	'transaction_balance_in_id',
    	'transaction_balance_out_id',
    	'date',
    	'credit',
    	'debit',
    	'balance',
    	'investment',
    	'status',
    	'information',
    	'active',
    	'is_deleted'
    ];
}
