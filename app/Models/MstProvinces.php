<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstProvinces extends Model
{
    protected $table = 'mst_provinces';

    protected $fillable = ['id','name'];

    public function regency()
    {
    	return $this->hasMany('App\Models\MstRegencies');
    }
}
