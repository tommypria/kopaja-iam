<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Modules\SecModule;

class SysUserLogCode extends Model
{
    protected $table = 'sys_user_log_code';

    protected $fillable = [
    	'code',
    	'module',
    	'description',
    ];

    public $timestamps = false;

    public $incrementing = false;

    public $primaryKey = 'code'; 

    public $keyType = 'string';

    public function module()
    {
    	return $this->belongsTo('App\Models\SecModule');
    }
}
