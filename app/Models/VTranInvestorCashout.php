<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class VTranInvestorCashout extends Model
{
    protected $table = 'v_tran_investor_cashout';

    public $primaryKey = 'id';

    public $timestamps = false;

    public $incrementing = false;

    public function countBalanceOut($status)
    {
    	return DB::select("
    		SELECT 
			    COUNT(*) AS total
			FROM
			    trc_transaction_balance_out
			WHERE
			    lower(status) = lower('".$status."');
    	")[0]->total;
        /*AND date BETWEEN CONCAT(SUBDATE(CURRENT_DATE(), 1),' ','21:00:00') 
        AND CONCAT(CURRENT_DATE(), ' ', '21:00:00');*/
    }
}
