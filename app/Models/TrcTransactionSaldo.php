<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrcTransactionSaldo extends Model
{
	public $timestamps = false;
	
    protected $table = "trc_transaction_saldo";

    protected $fillable = ['investor_id','inv_bank_id','invoice',
    						'date','due_date','status',
    						'amount','active','created_by_investor',
    						'created_at_investor','updated_by','updated_at',
    						'updated_by_investor','updated_at_investor','deleted_by',
    						'deleted_at','is_deleted'
    					];
}
