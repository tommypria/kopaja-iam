<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class BalanceTopup extends Model
{
    protected $fillable = [
    	'vinvestor',
    	'vtrunnumber',
    	'vdate',
    	'vamount',
    ];
}
