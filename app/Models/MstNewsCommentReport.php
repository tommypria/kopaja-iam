<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstNewsCommentReport extends Model
{
    protected $table = 'mst_news_comment_report';

    protected $fillable = [
    	'comment_id',
    	'reported',
    	'reported_by',
    	'action',
    	'status',
    	'active',
    	'action_by',
    	'created_date',
    	'is_deleted'
    ];

    public $timestamps = false;
}
