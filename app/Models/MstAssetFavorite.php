<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstAssetFavorite extends Model
{
    protected $table = 'mst_asset_favorite';

    protected $fillable = [
    	'asset_id',
    	'investor_id',
    	'comment',
    	'active',
    	'created_by',
    	'updated_by',
    	'deleted_by',
    	'deleted_at',
    	'is_deleted'
    ];
}
