<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MstMemberLang extends Model
{
    protected $table = 'mst_member_lang';

    public $timestamps = false;

    protected $fillable = [
    	'member_id',
    	'code',
    	'description',
    	'image'
    ];
}
